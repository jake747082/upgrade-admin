# encoding: utf-8
namespace :platform do
  desc "Platform histories import"
  task :histories => :environment do
    GamePlatforms::Histories.new('bt').import!
    GamePlatforms::Histories.new('bts').import!
    # GamePlatforms::Histories.new('bng').import!
    # GamePlatforms::Histories.new('east').import!
    GamePlatforms::Histories.new('wm').import!
    GamePlatforms::Histories.count_jackpot
  end

  task :lost_bt_histories => :environment do
    GamePlatforms::Histories.new('bt').import!(true)
  end

  task :agent_commission_reports => :environment do
    yesterday = (Time.now - 1.day).to_date.to_s
    Reports::AgentCommission.new(yesterday, yesterday)
  end
end
