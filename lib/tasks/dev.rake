# encoding: utf-8
namespace :dev do
  desc "Rebuild system"
  task :update => :environment do
    %w(db:migrate).each do |task_command|
      puts "Invoke: #{task_command} ..."
      Rake::Task[task_command].invoke
    end
  end
  task :build => :environment do
    %w(tmp:clear log:clear db:drop db:create db:migrate db:seed gambo:build db:schema:dump).each do |task_command|
      puts "Invoke: #{task_command} ..."
      Rake::Task[task_command].invoke
    end
  end
end