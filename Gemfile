
source 'http://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.7'

# Use mysql as the database for Active Record
gem 'mysql2', '~> 0.3.18'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.2'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.1'

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

# Use jquery as the JavaScript library
gem 'jquery-rails'

gem 'jquery-ui-sass-rails'

gem 'rails4-autocomplete'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.1.2'
gem 'oj'

# Style, Views, Template (libraries & tools)
gem 'bootstrap-sass-rails'
gem 'bootstrap-datepicker-rails'
gem 'will_paginate'
gem "bootstrap_helper", git: 'https://github.com/afunction/bootstrap-helper.git'
gem 'font-awesome-rails'
gem 'activerecord-mysql-unsigned'
gem 'rails_autolink', '~> 1.1.6'

gem 'cells', '3.11.3'
gem "haml-rails", "~> 0.9"
gem 'nprogress-rails'
gem 'zeroclipboard-rails'

# cron jobs
gem 'whenever', require: false

# Form Objects
gem 'reform'
gem 'virtus'

# SEO & Analytics
gem 'seo_helper', '~> 1.0.2'

# Forms & Helpers
gem "bootstrap_form"

# Export Excel
gem 'axlsx_rails'

# Models
gem 'super_accessors'
gem 'draper'
gem 'validates_email_format_of'
gem 'ipaddress'
gem 'dalli'
gem 'simple_enum'
gem 'state_machine'
gem 'soft_deletion'
gem 'awesome_nested_set'
gem 'stamp', '0.4.0' # does not work with newer versions
gem 'stamp-i18n'
gem 'public_activity', '1.5.0'

# Charts
gem 'chartkick'
gem 'groupdate'

# User System
gem 'devise'

# Others tools
gem 'aescrypt'
gem 'rails-i18n'
gem 'settingslogic'
gem 'actionpack-action_caching'
gem 'actionpack-page_caching'

# Server Solutions
gem 'puma', '2.11.1'
gem 'pusher'
gem 'memoist'

# ACL
gem 'pundit', '0.3.0'

# papertrail
gem 'remote_syslog_logger'

gem 'redactor-rails'
gem "carrierwave"
gem "mini_magick"

gem 'exception_notification', git: 'https://github.com/smartinez87/exception_notification.git'
gem 'slack-notifier'

# 所有環境都綁，包含 production
gem "pry-rails"
gem "awesome_print", require: false
gem 'visionbundles', git: 'git@gitlab.com:btsi365.corp/visionbundles.git', branch: 'master'

# gem 'gambo', git: 'git@gitlab.com:btsi365.corp/bt_lobby/gambo.git', branch: 'master'
gem 'gambo', git: 'git@gitlab.com:btsi365.corp/bt_lobby/gambo.git', branch: 'stage'
# gem 'gambo', git: 'git@gitlab.com:btsi365.corp/bt_lobby/gambo.git', branch: 'develop'
# gem 'gambo', path: '../gambo'

# soap
gem 'savon', '~> 2.11.0'

# http request元
gem 'rest-client'

gem 'faker', '1.9.1'

gem 'rake', '12.3.3'
gem 'minitest', '5.12.0'

gem 'dry-inflector', '0.1.2'
gem 'mime-types-data', '3.2019.1009'

# SMS
gem 'sms_mitake', git: 'git@gitlab.com:btsi365.corp/gems/sms_mitake.git', branch: 'master'

group :production do
  gem 'newrelic_rpm'
end

group :development, :test do # 也包含 test 是為了讓寫 test case 時也可以 debug
  gem "pry-plus"
  gem "hirb", require: false
  gem "hirb-unicode", require: false
  gem 'simplecov', require: false
  gem "pry-remote"
  gem 'capybara'
  gem 'poltergeist'
end

group :development do
  gem 'capistrano', '~> 2.15.5', require: false
  gem 'rvm-capistrano'
  gem 'binding_of_caller'
  gem 'better_errors'
  gem 'powder'
  gem 'database_cleaner'
  gem 'rspec-rails', '~> 3.4.2'
  gem 'parallel_tests'
end
