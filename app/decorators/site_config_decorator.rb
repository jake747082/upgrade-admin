class SiteConfigDecorator < Draper::Decorator
  delegate_all

  # 遊戲端
  def lobby_power_light
    lobby_maintenance? ? light_off : light_on
  end

  def short_lobby_marquee
    h.truncate(lobby_marquee, length: 60)
  end

  # 會員端
  def cash_power_light
    cash_maintenance? ? light_off : light_on
  end

  def short_cash_marquee
    h.truncate(cash_marquee, length: 60)
  end

  # 代理端
  def agent_power_light
    agent_maintenance? ? light_off : light_on
  end

  def short_agent_marquee
    h.truncate(agent_marquee, length: 60)
  end

  protected
    def light_on
      '<i class="fa fa-power-off font-on"></i>'.html_safe
    end

    def light_off
      '<i class="fa fa-power-off font-off"></i>'.html_safe
    end
end
