module UserHelper
  def render_credit(credit_rows, field)
    credit_rows.present? ? default_currency(credit_rows.first[field.to_sym]) : 0
  end
end