module HotMenuHelper

  # 狀態為已提交的申請存款數
  def pending_deposits_count
    Withdraw.list_deposit.list_apply.list_pending.size
  end

  # 狀態為已提交的申請提款數
  def pending_cashouts_count
    Withdraw.list_cashout.list_apply.list_pending.size
  end

  # 前天中午12:00~當日中午12:00 新註冊的會員數量
  def new_users_count
    begin_date = (Time.zone.now - 1.day).strftime('%Y-%m-%d') + ' 12:00:00'
    end_date   = Time.zone.now.strftime('%Y-%m-%d') + ' 11:59:59'
    User.where('created_at >= ? AND created_at < ?', begin_date, end_date).size
  end
end