json.username @user.username
json.nickname @user.nickname
if @user.current_sign_in_at.present?
  json.current_sign_in_at default_log_time(@user.current_sign_in_at)
end
json.current_sign_in_ip @user.current_sign_in_ip
json.credit_left default_currency(@user.credit_left)
json.cashout_limit default_currency(@user.cashout_limit)
json.bet_credit_sum default_currency(@user.bet_credit_sum)
json.lobby_id @user.lobby_id