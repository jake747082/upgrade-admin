class GamePolicy < ApplicationPolicy
  def all?
    false
    # admin.super_manager? || admin.game_manager?
  end

  def platform?
    admin.super_manager? || admin.game_manager?
  end
end
