class System::AdminPolicy < SystemPolicy
  def all?
    admin.super_manager?
  end
end