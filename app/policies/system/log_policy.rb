class System::LogPolicy < SystemPolicy
  def all?
    admin.super_manager?
  end
end