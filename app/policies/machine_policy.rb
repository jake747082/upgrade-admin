class MachinePolicy < ApplicationPolicy
  def all?
    admin.super_manager? && Setting.shop_site?
  end
end