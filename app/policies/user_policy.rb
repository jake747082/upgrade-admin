class UserPolicy < ApplicationPolicy

  def all?
    (admin.super_manager? || admin.main_manager? || admin.custom_service?) && !Setting.shop_site?
  end

  def index?
    all?
  end

  def new?
    (admin.super_manager? || admin.main_manager?) && Setting.cash_site?
  end

  def create?
    new?
  end

  def log?
    new? || admin.custom_service?
  end

  def update?
    new?
  end

  def group?
    new? && !admin.main_manager?
  end
end