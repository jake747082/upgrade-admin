class ReportPolicy < ApplicationPolicy
  def all?
    admin.super_manager? || admin.report_manager? || admin.main_manager?
  end
end