class FinancePolicy < ApplicationPolicy
  def all?
    admin.super_manager? || admin.report_manager? || admin.main_manager? || admin.custom_service?
  end

  def finances?
    !(admin.main_manager? || admin.custom_service?)
  end
end