class UsersCell < BaseCell
  # user row
  def list(args)
    @user = args[:user]
    render
  end
end