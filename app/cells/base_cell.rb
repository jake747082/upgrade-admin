class BaseCell < Cell::Rails
  include Pundit
  include Devise::Controllers::Helpers

  helper_method :current_admin
  alias_method :pundit_user, :current_admin
end