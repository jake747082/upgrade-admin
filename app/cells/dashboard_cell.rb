class DashboardCell < BaseCell
  helper ApplicationHelper
  include SiteHelper

  def information
    @remote_ip = remote_ip
    prepare_counter_data
    render
  end

  cache :site_status do |options|
    [ options[:site_config].updated_at, I18n.locale ]
  end
  def site_status(args)
    @site_config = args[:site_config].decorate
    render
  end

  def report_overview
    if params[:begin_date].nil? && params[:end_date].nil?
      @today_users_count = User.today.count
      @week_users_count = User.week.count
      @month_users_count = User.month.count
  
      @today_total_betform_sum = ThirdPartyBetForm.today.total_betform_sum
      @week_total_betform_sum = ThirdPartyBetForm.week.total_betform_sum
      @month_total_betform_sum = ThirdPartyBetForm.month.total_betform_sum

      @today_total_withdraw_sum = {}
      @week_total_withdraw_sum = {}
      @month_total_withdraw_sum = {}
      Withdraw.list_transferred.today.total_withdraw_sum.each { |sum| @today_total_withdraw_sum[sum.cash_type] = sum.total_credit }
      Withdraw.list_transferred.week.total_withdraw_sum.each { |sum| @week_total_withdraw_sum[sum.cash_type] = sum.total_credit }
      Withdraw.list_transferred.month.total_withdraw_sum.each { |sum| @month_total_withdraw_sum[sum.cash_type] = sum.total_credit }
    else
      @begin_date = params[:begin_date]
      @end_date = params[:end_date]
      @users_count = User.datetime_range(@begin_date, @end_date).count
      @total_betform_sum = ThirdPartyBetForm.datetime_range(@begin_date, @end_date).total_betform_sum
      @total_withdraw_sum = {}
      Withdraw.list_transferred.datetime_range(@begin_date, @end_date).total_withdraw_sum.each { |sum| @total_withdraw_sum[sum.cash_type].nil? ? @total_withdraw_sum[sum.cash_type] = sum.total_credit : @total_withdraw_sum[sum.cash_type] += sum.total_credit }
    end
    render
  end

  private

  def prepare_counter_data
    @total_users_count = Rails.cache.fetch(:home_users_counter, expires_in: 10.minute) do
      User.all.count
    end
    @total_agents_count = Rails.cache.fetch(:home_agents_counter, expires_in: 10.minute) do
      Agent.all.count
    end
  end
end