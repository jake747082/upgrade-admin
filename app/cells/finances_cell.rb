class FinancesCell < BaseCell
  helper ApplicationHelper

  # 日期區間選擇
  def filter_form(args)
    setup_form_data(args)
    render
  end

  # 報表顯示
  def cashout_list(args)
    setup_form_data(args)
    if @lobby_id.present?
      @cashout_stats = Finance::ChildrenCashout.new.get_user(@begin_date, @end_date, @user)
    else
      @cashout_stats = Finance::ChildrenCashout.call(@begin_date, @end_date, @agent)
    end
    render
  end

  private

  def setup_form_data(args)
    @begin_date = args[:begin_date]
    @end_date = args[:end_date]
    @lobby_id = args[:lobby_id]
    @agent = args[:agent] || nil

    @levels = if @lobby_id.present?
      user_id = User.lobby_id_to_id(@lobby_id)
      @user = user_id > 0 ? User.find_by_id(user_id) : nil
      @user.nil? ? nil : @user.agent.self_and_ancestors
    else
      @agent.nil? ? nil : @agent.self_and_ancestors
    end
  end
end