class WithdrawsController < Withdraw::BaseController
  before_action :set_withdraw, except: [:index, :digital_currency, :logs, :agent_logs]
  before_action :set_withdraw_type
  skip_before_action :set_breadcrumbs, only: [:index, :logs, :agent_logs]
  # GET /withdarws
  def index
    @withdraws = Withdraw.search(params[:query]).includes(:user).skip_ppp_type.withdraw_type(@type).page(params[:page])
    if params[:view_page] == 'digital_currency'
      drop_breadcrumb(t("breadcrumbs.withdraws.digital_currency"))
      render 'withdraws/digital_currency'
    else
      drop_breadcrumb(t("breadcrumbs.withdraws.cashouts"))
      render 'withdraws/index'
    end
  end

  # PUT /withdarws/1
  def update
    @before_balance = @withdraw.user.credit_left
    log_key = @type
    is_create_log = true
    if params[:status] == 'transferred'
      if @withdraw.type == :ace_pay
        is_create_log = false # 不新增log紀錄
        trans = User::TransWallet.new
        state = trans.cashout(@withdraw, 'QQ')
        error_code = trans.error_code
      else
        state = @withdraw.may_finish? ? @withdraw.finish!(withdraw_params) : false
      end
    elsif params[:status] == 'proccessing'
      state = @withdraw.may_proccess? ? @withdraw.proccess! : false
    else
      state = false
    end

    redirect_url(state, params[:status], @type, params[:view_page], is_create_log, error_code || 999)
  end

  # DELETE /withdarws/1
  def destroy
    @before_balance = @withdraw.user.credit_left
    state = if params[:status] == 'removed'
      @withdraw.may_remove? ? @withdraw.remove! : false
    elsif params[:status] == 'rejected'
      @withdraw.may_reject? ? @withdraw.reject! : false
    else
      false
    end
    redirect_url(state, params[:status], @type, params[:view_page])
  end

  def logs
    drop_breadcrumb(t('breadcrumbs.withdraws.logs'))
    logs = PublicActivity::Activity.where(key: ['user.cashout', 'user.deposit']).page(params[:page])
    @logs = []
    logs.each do |log|
      if log.trackable != nil
        @logs.push(log)
      end
    end
    render 'logs/index'
  end

  def show; end

  protected

  def withdraw_params
    params.require(:withdraw).permit(:note, :admin_note)
  end

  def set_breadcrumbs
    breadcrumb = "handle_#{params[:type]}"
    drop_breadcrumb(t("breadcrumbs.withdraws.#{breadcrumb}"))
  end

  def set_withdraw
    @withdraw = Withdraw.find(params[:id])
  end

  def set_withdraw_type
    @type = params[:type]
  end

  def redirect_url(state, status, type, view_page = '', is_create_log = true, error_status = 999)
    if state
      # save log
      @withdraw.user.create_log(@action || type, current_admin, remote_ip, status: status, credit: @withdraw.credit_actual.to_f, id: @withdraw.trade_no, withdraw_id: @withdraw.id, trade_no: @withdraw.trade_no, after_balance: get_user_after_balance(@withdraw.user), before_balance: @before_balance.to_f) if is_create_log
      redirect_to withdraws_url(type: type, view_page: view_page), notice: t("flash.withdraw.#{status}")
    else
      redirect_to withdraws_url(type: type, view_page: view_page), alert: t("flash.withdraw.#{status}_faild", error_status: error_status)
    end
  end
end
