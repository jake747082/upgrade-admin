class Reports::UsersController < ApplicationController
  include DateRangeFilter
  before_action :valid_permission!, :set_default_date_range
  before_action :set_breadcrumbs_path, only: :index
  before_action :verify_user, only: [:index, :details]
  before_action :set_agent, only: :details

  def index
    if @user
      withdraw_sum = Withdraw.includes(:currency).cash_reports(@begin_date, @end_date).where(user: @user)
      details = Withdraw.includes(:currency).where(updated_at: @begin_date..@end_date).list_transferred.list_by_digital_currency.where(user: @user)
    else
      withdraw_sum = Withdraw.cash_reports(@begin_date, @end_date)
      details = Withdraw.includes(:currency).where(updated_at: @begin_date..@end_date).list_transferred.list_by_digital_currency.includes(:user)
    end

    all_currency_name = ["USDT_ERC20", "IBC", "UNI", "USDG"]
    @all_currency = {}
    all_currency_name.each do |currency|
      @all_currency.merge!({currency => {:dc_piece_deposit => 0.0, :dc_point_deposit => 0.0, :extra_bouns => 0.0, :dc_piece_cashout => 0.0, :dc_pount_cashout => 0.0}})
    end

    details.each do |detail|
      currency = detail.currency.name
      if @all_currency.has_key?(currency)
        if detail[:cash_type_cd] == 0
          @all_currency[currency][:dc_piece_deposit] += detail.amount
          @all_currency[currency][:dc_point_deposit] += detail.credit
          @all_currency[currency][:extra_bouns] += detail.credit_diff
        else
          @all_currency[currency][:dc_piece_cashout] += detail.amount
          @all_currency[currency][:dc_pount_cashout] += detail.credit
        end
      end
    end

    # 主控台 > 財務總結 > 左側
    @withdraw = {dc_deposit: 0, dc_cashout: 0, deposit: 0, cashout: 0, extra_deposit: 0}
    withdraw_sum.each do |withdraw|
      # 存款
      if withdraw.cash_type == :deposit
        case withdraw.type
        when :admin
          @withdraw[:deposit] += withdraw.credit_actual
        when :extra_deposit
          @withdraw[:extra_deposit] += withdraw.credit_actual
        when :ace_pay
          @withdraw[:dc_deposit] += withdraw.credit
          @withdraw[:extra_deposit] += withdraw.credit_diff
        # when :comission
          #@withdraw[:comission] += withdraw.credit_actual
        # when :event_offer
          #@withdraw[:event_offer] += withdraw.credit_actual
        end
      # 提款
      else
        case withdraw.type
        when :admin
          @withdraw[:cashout] -= withdraw.credit_actual
        when :extra_deposit
          @withdraw[:extra_deposit] -= withdraw.credit_actual
        when :ace_pay
          # @withdraw[:dc_piece_cashout] -= withdraw.amount
          @withdraw[:dc_cashout] -= withdraw.credit_actual
        # when :comission
          #@withdraw[:comission] -= withdraw.credit_actual
        # when :event_offer
          #@withdraw[:event_offer] -= withdraw.credit_actual
        end
      end
    end
  end

  # 所有details裡的footer總計欄位
  def details
    drop_breadcrumb(t('breadcrumbs.reports.users'), reports_users_path(params))
    drop_breadcrumb(t("breadcrumbs.reports.user.#{params[:type]}") + (@user ? " ( #{@user.lobby_id} )" : ''))
    @sum = {deposit: {credit_actual: 0, credit: 0, amount: 0, credit_diff: 0}, cashout: {credit_actual: 0, credit: 0, amount: 0, credit_diff: 0}, extra_deposit: {credit_actual: 0, credit: 0, amount: 0, credit_diff: 0}, dc_deposit: {credit_actual: 0, credit: 0, amount: 0, credit_diff: 0}, dc_cashout: {credit_actual: 0, credit: 0, amount: 0, credit_diff: 0}}
    @details  = Withdraw.includes(:currency).list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).list_transferred.includes(:user)
    @cash_reports = Withdraw.includes(:currency).cash_reports(@begin_date, @end_date).list_by_type(params[:type])
      if params[:currency]
        currency_id = Currency.where(name: params[:currency])[0][:id] 
        @details = @details.where(currency_id: currency_id)
        @cash_reports = @cash_reports.where(currency_id: currency_id)
      end
      if @user
        @details = @details.where(user_id: @user.id)
        @cash_reports = @cash_reports.where(user_id: @user.id)
      end
      if @agent
        @details = @details.where("#{@agent.agent_level}_id = ? ", @agent.id)
        @cash_reports = @cash_reports.where("#{@agent.agent_level}_id = ? ", @agent.id)
      end
      @details = @details.page(params[:page])
      @details.each do |cash_report|
        if cash_report.type == :admin
          if  cash_report.cash_type == :deposit
            @sum[cash_report.cash_type][:credit_actual] += cash_report.credit_actual
            @sum[cash_report.cash_type][:credit] += cash_report.credit
          else 
            @sum[cash_report.cash_type][:credit_actual] -= cash_report.credit_actual
            @sum[cash_report.cash_type][:credit] -= cash_report.credit
          end
        else
          if cash_report.cash_type == :deposit
            @sum[:extra_deposit][:credit_diff] += cash_report.credit_diff
              if cash_report.type == :ace_pay
                @sum[:dc_deposit][:credit_actual] += cash_report.credit_actual
                @sum[:dc_deposit][:credit] += cash_report.credit
                @sum[:dc_deposit][:credit_diff] += cash_report.credit_diff
                @sum[:dc_deposit][:amount] += cash_report.amount
              end
          else
            @sum[:extra_deposit][:credit_diff] -= cash_report.credit_diff
              if cash_report.type == :ace_pay 
                @sum[:dc_cashout][:credit_actual] -= cash_report.credit
                @sum[:dc_cashout][:amount] += cash_report.amount
              end
          end
        end
      end
  end

  protected  
  def verify_user
    @lobby_id ||= params[:lobby_id]
    user_id = User.lobby_id_to_id(params[:lobby_id])
    unless params[:lobby_id].nil? || params[:lobby_id].empty? || user_id.nil? || @user = User.find_by_id(user_id)
      redirect_to reports_users_path , alert: t('flash.lobby_id.invalid')
    end
  end

  def set_agent
    unless params[:agent_id].nil? || params[:agent_id].empty?
      @agent = Agent.find_by_id(params[:agent_id])
    end
  end

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.reports.users'), reports_users_path)
  end

  def valid_permission!
    authorize :report, :all?
  end
end