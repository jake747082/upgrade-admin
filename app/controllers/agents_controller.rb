class AgentsController < Agent::BaseController
  before_action :set_agent, only: [:show, :edit, :update, :lock, :unlock, :get_children]

  # GET /agents
  def index
    @agents = Agent.search(params[:query]).page(params[:page])
  end

  # GET /agents/1
  def show

    respond_to do |format|
      drop_breadcrumb(@agent.title)
  
      # date
      from_date = Date.today - 7.days
      end_date = Date.today + 1.days
  
      @logs = @agent.change_logs.where(key: 'agent.cashout').date_range(from_date, end_date).page(1)
      @cashout_stats = Finance::DailyCashout.call(begin_date: from_date, end_date: end_date, agent: @agent)
      @withdraw_stats = Withdraw::DailyReport.call(begin_date: from_date, end_date: end_date, agent: @agent)
      format.html
      format.json {
        render "agents/show.json"
      }
    end
  end

  # GET /agents/new
  def new
    @form = AgentForm::Create.new(Agent.new)
    drop_breadcrumb("#{t('breadcrumbs.agent.new')} - #{@form.model.human_agent_level}")
  end

  # POST /agents
  def create
    @form = AgentForm::Create.new(Agent.new)
    if @form.create(params[:agent])
      @form.model.create_log(:create, current_admin, remote_ip)
      redirect_to @form, notice: t('flash.shared.create.finish')
    else
      render :new
    end
  end

  # GET /agents/1/edit
  def edit
    @form = AgentForm::Update.new(@agent)
    @type = @agent[:type_cd]
  end

  # PATCH/PUT /agents/1
  def update
    @form = AgentForm::Update.new(@agent)
    if @form.update(params[:agent])
      @agent.create_log(:update, current_admin, remote_ip)
      redirect_to edit_agent_path(@agent), notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  def get_children
    respond_to do |format|
      format.json { render json: @agent.children.select(:id, :nickname).to_json }
    end
  end
end