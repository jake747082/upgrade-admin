class Withdraw::StatusesController < Withdraw::BaseController
  protect_from_forgery except: [:funpay]
  require 'net/https'
  # GET /withdraw/:status
  def show
    @type = params[:type]
    status = params[:status].to_s.downcase.to_sym

    if Withdraw.statuses[status].blank?
      return render_404!
    end

    @withdraws = Withdraw.where(status: Withdraw.statuses[status]).includes(:user).search(params[:query]).page(params[:page]).withdraw_type(@type)
    if params[:view_page] == 'digital_currency'
      render 'withdraws/digital_currency'
    else
      render 'withdraws/index'
    end
  end

  def funpay
    @withdraws = Withdraw.find(params[:cashout_id])
    bank_name = t("bank_accounts.bank_title.#{@withdraws.bank_account_info[:bank_id]}")
    @data = {
      :biz_no => "cashout#{@withdraws.trade_no}",
      :total_amount => @withdraws.credit.to_i*100,
      :total_count => 1,
      :request_time => Time.now.strftime("%Y%m%d%H%M%S"),
      :pay_item => "cashout#{@withdraws.trade_no},#{@withdraws.bank_account_info[:title]},#{@withdraws.bank_account_info[:account]},#{@withdraws.credit.to_i*100},#{@withdraws.user.mobile},,,#{bank_name},#{@withdraws.bank_account_info[:province]},#{@withdraws.bank_account_info[:city]},#{@withdraws.bank_account_info[:subbranch]},1",
    }
    str = "MERCHANT_CODE=" + Setting.funpay.merchant_code + "&BIZ_NO=" + @data[:biz_no] + "&TOTAL_AMOUNT=#{@data[:total_amount]}&TOTAL_COUNT=#{@data[:total_count]}&REQUEST_TIME=" + @data[:request_time] + "&PAY_ITEM=" + @data[:pay_item] + "&VERSION=" + Setting.funpay.version + "&CHARSET=" + Setting.funpay.charset + "&SIGN_TYPE=" + Setting.funpay.sign_type + "&pkey=" + Setting.funpay.pkey
    @data[:sign_value] = Digest::MD5.hexdigest(str)
    uri = URI.parse(Setting.funpay.url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(uri.request_uri)
    request.set_form_data({
      MERCHANT_CODE: Setting.funpay.merchant_code, 
      BIZ_NO: @data[:biz_no], 
      TOTAL_AMOUNT: @data[:total_amount], 
      TOTAL_COUNT: @data[:total_count],
      REQUEST_TIME: @data[:request_time],
      PAY_ITEM: @data[:pay_item],
      VERSION: Setting.funpay.version,
      CHARSET: Setting.funpay.charset,
      SIGN_TYPE: Setting.funpay.sign_type,
      SIGNVALUE: @data[:sign_value]
    })
    res = URI.decode(URI.escape(http.request(request).body))
    @data_arr={}
    spilt_str = res.split('&')
    spilt_str.map{ |str| str.split('=') }.each{ |val| @data_arr[ val[0] ] = val[1].nil? ? '' : val[1] }
    item = {}
    back_order = [ "order_id", "funpay_seq_no", "status", "amount", "error_code", "error_msg" ]
    @data_arr['PAY_ITEM'].split(',').each_with_index{ |val, index| item[ back_order[index] ] = val.nil? ? '' : val }
    @data_arr['ITEM'] = item
    # if void_back_data
      if item['status'] == "1"
        @withdraws.finish!
        @withdraws.update(status: 2, deposit_at: Time.now, order_info: @data_arr.to_json, admin_note: item['error_msg'] || @data_arr['ERROR_MSG'])
        @withdraws.user.create_log(:cashout, current_admin, remote_ip, status: 'transferred', credit: @withdraws.credit_actual.to_f, id: @withdraws.trade_no, order_no: item["order_id"], pay_credit: item["amount"].to_i/100)
        redirect_to withdraws_url(type: 'cashout'), notice: item['error_msg'] || @data_arr['ERROR_MSG']
      else
        @withdraws.remove!
        @withdraws.update(status: 4, deposit_at: Time.now, order_info: @data_arr.to_json, admin_note: item['error_msg'] || @data_arr['ERROR_MSG'])
        @withdraws.user.create_log(:cashout, current_admin, remote_ip, status: 'rejected', credit: @withdraws.credit_actual.to_f, id: @withdraws.trade_no)
        redirect_to withdraws_url(type: 'cashout'), alert: item['error_msg'] || @data_arr['ERROR_MSG']
      end
    # else
    #   @withdraws.remove!
    #   @withdraws.update(status: 4, deposit_at: Time.now, order_info: @data_arr.to_json, admin_note: item['error_msg'] || @data_arr['ERROR_MSG'])
    #   @withdraws.user.create_log(:cashout, current_admin, remote_ip, status: 'rejected', credit: @withdraws.credit_actual.to_f, id: @withdraws.trade_no)
    #   # @withdraws.cancel!
    #   redirect_to withdraws_url(type: 'cashout'), alert: item['error_msg'] || @data_arr['ERROR_MSG']
    # end
  end

  protected

  def set_breadcrumbs
    drop_breadcrumb(t("breadcrumbs.withdraws.handle_#{params[:type]}"))
  end

  def void_back_data
    back_str = "MERCHANT_CODE=" + Setting.funpay.merchant_code + "&BIZ_NO=" + @data_arr['BIZ_NO'] + "&TOTAL_AMOUNT=" + @data_arr['TOTAL_AMOUNT'] + "&TOTAL_COUNT=" + @data_arr['TOTAL_COUNT'] + "&SUCCESS_AMOUNT=" + @data_arr['SUCCESS_AMOUNT'] + "&SUCCESS_COUNT=" + @data_arr['SUCCESS_COUNT'] + "&ERROR_CODE=" + @data_arr['ERROR_CODE'] + "&ERROR_MSG=" + @data_arr['ERROR_MSG'] + "&PAY_ITEM=" + @data_arr['PAY_ITEM'] + "&pkey=" + Setting.funpay.pkey
    Digest::MD5.hexdigest(back_str) == @data_arr['SIGNVALUE'] ? true : false
  end
end
