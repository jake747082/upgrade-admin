class Withdraw::BaseController < ApplicationController
  before_action :valid_permission!, :set_breadcrumbs

  protected

  def valid_permission!
    authorize :withdraws, :all?
  end

  # 計算充值後包含其他多錢包總額度
  def get_user_after_balance(user)
    service = GamePlatforms::UserBalance.new(user)
    after_balance = if user_balance = service.get_balance
      (user_balance + user.reload.credit_left).to_f
    else
      -1
    end
  end
end