class Withdraw::CashoutsController < Withdraw::BaseController
  before_action :set_user, only: :create
  before_action :set_withdraw
  autocomplete :user, :username
  # GET /withdraw/:cashouts/new
  def new; end

  def show
    @withdraw = Withdraw.includes(:user).find(params[:id])
    @tx_id = WalletLog.where(withdraw_id: params[:id])
  end

  def create
    @before_balance = @withdraw.user.credit_left unless @withdraw.user.nil?
    if @withdraw.cashout
      @withdraw.user.create_log(:cashout, current_admin, remote_ip ,credit: @withdraw.credit_actual.to_f, withdraw_id: @withdraw.withdraw_id, trade_no: @withdraw.model.trade_no, status: 'transferred', after_balance: get_user_after_balance(@withdraw.user), before_balance: @before_balance.to_f, type: @withdraw.model.type_cd, cash_type: @withdraw.model.cash_type_cd)
      redirect_to new_withdraw_cashout_path, notice: t('flash.withdraw.cashouts.finish')
    else
      @id = @user.nil? ? '' : @user.lobby_id
      render :new
    end
  end

  protected

  def set_breadcrumbs
    drop_breadcrumb(t('breadcrumbs.withdraws.cashouts'))
  end

  def set_user
    user_id = User.lobby_id_to_id(params[:id])
    @user = User.find_by_id(user_id) if user_id > 0
  end

  def set_withdraw
    params[:withdraw] ||= {}
    params[:withdraw].merge!(user: @user) unless @user.nil?
    @withdraw = WithdrawForm::Cashout.new(params[:withdraw].merge(trans_id: "M#{Time.zone.now.strftime("%Y%m%d%H%M%S")}#{SecureRandom.hex(3)}"))
  end

  def set_params
    params.require(:withdraw).permit(:username, :credit, :credit_diff, :note, :type, :cash_type)
  end
end
