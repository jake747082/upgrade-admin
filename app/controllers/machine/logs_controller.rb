class Machine::LogsController < Machine::BaseController
  before_action :set_machine

  # GET /machines/1/logs
  def index
    drop_breadcrumb(@machine.title)
    @logs = @machine.change_logs.page(params[:page])
    render 'logs/index'
  end
end