module System
  class LogsController < BaseController
    before_action :set_breadcrumbs_path

    # GET /logs
    def index
      logs = if params[:search].present?
        @owner     ||= params[:search][:owner]
        @trackable ||= params[:search][:trackable]
        @action    ||= params[:search][:action]
        @ip        ||= params[:search][:ip]
        @date      ||= params[:search][:date]
        
        query = PublicActivity::Activity.search(params[:search]).page(params[:page])
      elsif params[:query].present?
        users = User.where("username LIKE ? OR nickname LIKE ?", "%#{params[:query]}%", "%#{params[:query]}%")
        PublicActivity::Activity.where(trackable: users).page(params[:page])
      else
        PublicActivity::Activity.all.page(params[:page])
      end
      @logs = []
      logs.each do |log|
        if log.trackable != nil
          @logs.push(log)
        end
      end
      render 'logs/index'
    end

    protected

    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.logs.action_logs'), system_logs_path)
    end

    def valid_permission!
      authorize :'system/log', :all?
    end
  end
end