module System
  class PlatformGamesController < BaseController
    before_action :set_breadcrumbs_path

    def index
      @games_list = PlatformGame.all.includes(:game_platform)
    end

    def edit
      # @games_list = PlatformGame.all.includes(:game_platform)
      @platform_game = PlatformGame.find(params[:id])
    end

    def update
      @platform_game = PlatformGame.find(params[:id])
      befor_jp = @platform_game.jp_md.dup
      befor_bet_total_credit = @platform_game.bet_total_credit.dup
      logjp = platform_game_params[:jp_rate] != @platform_game.jp_rate || platform_game_params[:base_jp] != @platform_game.base_jp
      if @platform_game.update(platform_game_params)
        redirect_to system_platform_games_path, notice: t('flash.pages.update.finish')
        if(logjp)
        jp_logs = JpChangeLog.create({
          :manual => true,
          :befor_jp_change => befor_jp,
          :after_jp_change => @platform_game.jp_md,
          :befor_bet_total_credit => befor_bet_total_credit,
          :after_bet_total_credit => @platform_game.bet_total_credit,
          :jp_change => @platform_game.jp_md - befor_jp,
          :jp_rate => @platform_game.jp_rate,
          :platform_game => @platform_game
          })
        end
      else
        render :edit
      end

    end

    def update_is_jp
      respond_to do |format|
        format.js {
          if params[:id].to_i > 0
            @game = PlatformGame.find(params[:id])
            from_enable = @game.is_jp
            @status = @game.update!(is_jp: params[:is_jp])
          else
            @status = false
          end
          # current_admin.create_log(:platform_game_config, current_admin, remote_ip, platform_game: @game.code, from_enable: from_enable, to_enable: @game.enable, update_status: @status)
        }
      end
    end

    def update_enable
      respond_to do |format|
        format.js {
          if params[:id].to_i > 0
            @game = PlatformGame.find(params[:id])
            from_enable = @game.enable
            @status = @game.update!(enable: params[:enable])
          else
            @status = false
          end
          current_admin.create_log(:platform_game_config, current_admin, remote_ip, platform_game: @game.code, from_enable: from_enable, to_enable: @game.enable, update_status: @status)
        }
      end
    end

    def update_play_free
      respond_to do |format|
        format.js {
          if params[:id].to_i > 0
            @game = PlatformGame.find(params[:id])
            from_enable = @game.play_free
            @status = @game.update!(play_free: params[:play_free])
          else
            @status = false
          end
          # current_admin.create_log(:platform_game_config, current_admin, remote_ip, platform_game: @game.code, from_enable: from_enable, to_enable: @game.enable, update_status: @status)
        }
      end
    end

    def update_all_jp_rate
      PlatformGame.update_all(jp_rate: params[:data][:rate])
      redirect_to system_platform_games_path, notice: t('flash.pages.update.finish')
    end
    def update_all_jp
      GamePlatforms::Histories.new('bt').import!
      GamePlatforms::Histories.new('bts').import!
      # GamePlatforms::Histories.new('east').import!
      # GamePlatforms::Histories.new('wm').import!
      GamePlatforms::Histories.count_jackpot
      redirect_to system_platform_games_path, notice: t('flash.pages.update.finish')
    end
    def update_game_list
      @res = GamePlatforms::Games.update_games_list
    end

    private
    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.setting.platform_games'), system_platform_games_path)
    end

    def platform_game_params
      params.require(:platform_game).permit(:is_jp, :jp_rate,:game_reset,:base_jp,:multiple_display,:multiple)
    end

  end
end