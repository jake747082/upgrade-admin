module System
  module Admin
    class LogsController < System::BaseController
      before_action :set_admin, :set_breadcrumbs_path

      # GET /admin/1/logs
      def index
        drop_breadcrumb("#{@admin.title} - #{t('breadcrumbs.logs.action_logs')}")
        @logs = @admin.action_logs.page(params[:page])
        render 'logs/index'
      end

      # GET /admin/1/logs/change
      def change
        drop_breadcrumb("#{@admin.title} - #{t('breadcrumbs.logs.change_logs')}")
        @logs = @admin.change_logs.page(params[:page])
        render 'logs/index'
      end

      private

      def set_admin
        @admin = ::Admin.find(params[:admin_id])
      end

      def set_breadcrumbs_path
        drop_breadcrumb(t('breadcrumbs.admin.root'), system_admins_path)
      end
    end
  end
end