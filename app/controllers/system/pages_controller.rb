module System
  class PagesController < BaseController
    before_action :set_breadcrumbs_path
    before_action :set_page, only: [:edit, :update]

    # GET /pages
    def index
      @pages = Page.all
    end

    # GET /page/:page_id/edit
    def edit; end

    # PATCH/PUT /page/:page_id
    def update
      if @page.update(page_params)
        redirect_to system_pages_url, notice: t('flash.pages.update.finish')
      else
        render :edit
      end

    end

    protected

    def set_page
      @page = Page.find(params[:id])
    end

    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.static_pages'), system_pages_path)
    end

    def page_params
      params.require(:page).permit(:title, :content)
    end

    def valid_permission!
      authorize :'system/page', :all?
    end
  end
end