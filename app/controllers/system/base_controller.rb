module System
  class BaseController < ApplicationController
    before_action :valid_permission!

    protected

    def valid_permission!
      authorize :system, :all?
    end
  end
end