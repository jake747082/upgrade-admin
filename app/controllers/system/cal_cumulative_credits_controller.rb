class System::CalCumulativeCreditsController < System::BaseController

  def create
    respond_to do |format|
      format.js {
        UserDailyCreditLog::Import.call
        current_admin.create_log(:cal_cumulative_credits, current_admin, remote_ip)
      }
    end
  end

end