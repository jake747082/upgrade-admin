module System
  class AdminsController < BaseController
    before_action :set_breadcrumbs_path
    before_action :set_admin, only: [:edit, :update, :lock, :unlock]


    # POST /admins/1/lock|unlock
    def lock
      if @admin.update!(lock: true)
        @admin.create_log(:lock, current_admin, remote_ip)
        redirect_to edit_system_admin_url, notice: t('flash.admin.lock')
      else
        render :edit
      end
    end

    def unlock
      if @admin.update!(lock: false)
        @admin.create_log(:unlock, current_admin, remote_ip)
        redirect_to edit_system_admin_url, notice: t('flash.admin.unlock')
      else
        render :edit
      end
    end

    # GET /admins
    def index
      @admins = ::Admin.all.page(params[:page])
    end

    # GET /admins/new
    def new
      drop_breadcrumb(t('breadcrumbs.admin.new'), new_system_admin_path)
      @admin = ::Admin.new
    end

    # POST /admins
    def create
      @admin = ::Admin.new(add_admin_params)
      if @admin.save
        @admin.create_log(:create, current_admin, remote_ip)
        redirect_to system_admins_url, notice: t('flash.shared.create.finish')
      else
        render :new
      end
    end

    # GET /admins/1/edit
    def edit
      drop_breadcrumb(t('breadcrumbs.admin.edit'), edit_system_admin_path(@admin))
    end

    # PATCH/PUT /admins/1
    def update
      if @admin.update(update_admin_params)
        @admin.create_log(:update, current_admin, remote_ip)
        redirect_to system_admins_url, notice: t('flash.shared.update.finish')
      else
        render :edit
      end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin
      @admin = ::Admin.find(params[:id])
    end

    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.admin.root'), system_admins_path)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def add_admin_params
      params.require(:admin).permit(:nickname, :username, :password, :password_confirmation, :role)
    end

    def update_admin_params
      update_params = params.require(:admin).permit(:nickname, :username, :password, :password_confirmation)

      if update_params[:password].blank? && update_params[:password_confirmation].blank?
        update_params.delete(:password)
        update_params.delete(:password_confirmation)
      end
      update_params
    end

    def valid_permission!
      authorize :'system/admin', :all?
    end
  end
end