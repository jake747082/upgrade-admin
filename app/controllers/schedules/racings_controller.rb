class Schedules::RacingsController < Schedules::BaseController
  before_action :set_agent_breadcrumbs_path

  def index
    @racing_schedules = RacingSchedule.page(params[:page])
  end
end