class Products::DisablesController < Products::BaseController
  before_action :set_product

  # POST /products/:product_id/disables
  def create
    if @product.update(disable: true)
      # save log
      current_admin.create_log(:product_disable, current_admin, remote_ip, id: @product.id, name: @product.name)
      redirect_to products_path, notice: t('flash.shared.update.finish')
    else
      redirect_to products_path, alert: t('flash.shared.update.fail')
    end
  end

  # DELETE /products/:product_id/disables
  def destroy
    if @product.update(disable: false)
      # save log
      current_admin.create_log(:product_enable, current_admin, remote_ip, id: @product.id, name: @product.name)
      redirect_to products_path, notice: t('flash.shared.update.finish')
    else
      redirect_to products_path, alert: t('flash.shared.update.fail')
    end
  end

  protected

  def set_breadcrumbs
    drop_breadcrumb(t('breadcrumbs.withdraws.products'))
  end

  def set_product
    @product = Product.find(params[:product_id])
  end
end
