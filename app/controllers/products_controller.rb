class ProductsController < Products::BaseController
  before_action :set_product, only: [:edit, :update, :destroy]

  # GET /withdraw/products
  def index
    @products = Product.all.page(params[:page]).order(:cash)
  end

  def new
    @product = Product.new()
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      # save log
      current_admin.create_log(:product, current_admin, remote_ip, id: @product.id, name: @product.name)
      redirect_to products_path, notice: t('flash.shared.create.finish')
    else
      render :new
    end
  end

  def edit; end

  def update
    if @product.update(product_params)
      current_admin.create_log(:product_update, current_admin, remote_ip, id: @product.id, name: @product.name)
      redirect_to products_path, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  def destroy
    if @product.update(deleted: true)
      redirect_to products_path, notice: t('flash.shared.delete.finish')
    else
      redirect_to products_path, alert: t('flash.shared.delete.fail')
    end
  end

  protected

  def set_breadcrumbs
    drop_breadcrumb(t('breadcrumbs.withdraws.products'))
  end

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :sku, :cash, :credit)
  end

  def valid_permission!
    authorize :withdraws, :all?
  end
end
