class Game::LittleMary::MachinesController < Game::BaseController
  before_action :set_breadcrumbs_path, :set_cycle_sizes
  before_action :set_machine, only: [:edit, :update, :open, :close]
  skip_before_action :valid_not_api_site!

  # GET /games/little_mary/machines
  def index
    @machines = LittleMaryMachine.all
  end

  # GET /games/little_mary/machines/1/edit
  def edit
    render 'game/edit'
  end

  # PUT/PATCH /little_mary/machines/1
  def update
    if @machine.update(machine_params)
      redirect_to game_little_mary_machines_url, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.setting.little_mary_machines'), game_little_mary_machines_path)
  end

  def set_cycle_sizes
    @cycle_sizes = LittleMaryMachine::CYCLE_SIZES
  end

  def set_machine
    @machine = LittleMaryMachine.find(params[:id])
    drop_breadcrumb(@machine.human_game_name)
  end

  def machine_params
    params.require(:little_mary_machine).permit(:win_speed, :lose_rate_xs, :lose_rate_sm, :lose_rate_md, :lose_rate_lg, :lose_rate_xl,
                                         :lose_trigger_amount_xs, :lose_trigger_amount_sm, :lose_trigger_amount_md, :lose_trigger_amount_lg, :lose_trigger_amount_xl,
                                         :max_mutiple_xs, :max_mutiple_sm, :max_mutiple_md, :max_mutiple_lg, :max_mutiple_xl, :version)
  end
end