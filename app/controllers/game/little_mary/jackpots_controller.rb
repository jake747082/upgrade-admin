class Game::LittleMary::JackpotsController < Game::BaseController
  before_action :set_breadcrumbs_path, :set_little_mary_setting

  # GET /games/little_mary/jp
  def show; end

  # GET /games/little_mary/jp/edit
  def edit; end

  # PUT/PATCH /games/little_mary/jp
  def update
    @little_mary_settings.assign_attributes(little_mary_setting_params)

    if @little_mary_settings.save
      redirect_to game_little_mary_jackpot_url, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  private
    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.setting.little_mary_machines'), game_little_mary_machines_path)
      drop_breadcrumb(t('breadcrumbs.setting.jackpots'), game_little_mary_jackpot_path)
    end

    def set_little_mary_setting
      @little_mary_settings = GameSetting.find(2)
    end

    def little_mary_setting_params
      params.require(:little_mary_settings).permit(:jackpot_accumulate_rate)
    end
end