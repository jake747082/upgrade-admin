class Game::FishingJoy::MachinesController < Game::BaseController
  before_action :set_breadcrumbs_path, :valid_not_shop_site!
  before_action :set_machine, only: [:edit, :update, :open, :close]
  skip_before_action :valid_shop_site!, :valid_not_api_site!

  # GET /games/fishing_joy/machines
  def index
    @machines = FishingJoyMachine.all
  end

  # GET /games/fishing_joy/machines/1/edit
  def edit; end

  # PUT/PATCH /fishing_joy/machines/1
  def update
    if @machine.update(machine_params)
      redirect_to game_fishing_joy_machines_url, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.setting.fishing_joy_machines'), game_fishing_joy_machines_path)
  end

  def set_machine
    @machine = FishingJoyMachine.find(params[:id])
  end

  def machine_params
    params.require(:fishing_joy_machine).permit(:lose_rate, :version)
  end
end