class Game::Slot::MachinesController < Game::BaseController
  before_action :set_breadcrumbs_path, :set_cycle_sizes
  before_action :set_machine, only: [:edit, :update, :open, :close]
  skip_before_action :valid_not_api_site!

  # GET /games/slot/machines
  def index
    @machines = SlotMachine.all
  end

  # GET /games/slot/machines/1/edit
  def edit
    render 'game/edit'
  end

  # PUT/PATCH /slot/machines/1
  def update
    if @machine.update(machine_params)
      redirect_to game_slot_machines_url, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  # GET /games/slot/machines/all/edit_all
  def edit_all
    drop_breadcrumb(t('breadcrumbs.setting.edit_all_slot_machines'))
    @default_value = SlotMachine.find(1)
  end

  # PUT /games/slot/machines/all/update_all
  def update_all
    if SlotMachine.all.update_all(all_machine_params)
      redirect_to game_slot_machines_url, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.setting.slot_machines'), game_slot_machines_path)
  end

  def set_cycle_sizes
    @cycle_sizes = SlotMachine::CYCLE_SIZES
  end

  def set_machine
    @machine = SlotMachine.find(params[:id])
    drop_breadcrumb(@machine.human_game_name)
  end

  def all_machine_params
    params.permit(:win_speed, :lose_rate_xs, :lose_rate_sm, :lose_rate_md, :lose_rate_lg, :lose_rate_xl,
                                         :lose_trigger_amount_xs, :lose_trigger_amount_sm, :lose_trigger_amount_md, :lose_trigger_amount_lg, :lose_trigger_amount_xl,
                                         :max_mutiple_xs, :max_mutiple_sm, :max_mutiple_md, :max_mutiple_lg, :max_mutiple_xl)
  end

  def machine_params
    params.require(:slot_machine).permit(:win_speed, :lose_rate_xs, :lose_rate_sm, :lose_rate_md, :lose_rate_lg, :lose_rate_xl,
                                         :lose_trigger_amount_xs, :lose_trigger_amount_sm, :lose_trigger_amount_md, :lose_trigger_amount_lg, :lose_trigger_amount_xl,
                                         :max_mutiple_xs, :max_mutiple_sm, :max_mutiple_md, :max_mutiple_lg, :max_mutiple_xl, :version)
  end
end