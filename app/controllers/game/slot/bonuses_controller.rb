class Game::Slot::BonusesController < Game::BaseController
  before_action :set_breadcrumbs_path

  # GET /game/slot/bonus
  def index
    @bonuses = Bonus.all
  end

  # DELETE /game/slot/bonus
  def destroy
    if Bonus::Remove.call(params[:id])
      redirect_to :back, notice: t('flash.bonus.delete.finish')
    else
      redirect_to :back, alert: t('flash.bonus.delete.fail')
    end
  end

  private
    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.setting.slot_machines'), game_slot_machines_path)
      drop_breadcrumb(t('breadcrumbs.setting.bonus'), game_slot_bonuses_path)
    end
end