class Game::BaseController < ApplicationController
  before_action :valid_permission!, :valid_not_api_site!

  def open
    if @machine.update(open: true)
      current_admin.create_log(:machine_open, current_admin, remote_ip, {machine: @machine.human_game_name})
      redirect_to :back, notice: t('flash.shared.update.finish')
    else
      redirect_to :back
    end
  end

  def close
    if @machine.update(open: false)
      current_admin.create_log(:machine_close, current_admin, remote_ip, {machine: @machine.human_game_name})
      redirect_to :back, notice: t('flash.shared.update.finish')
    else
      redirect_to :back
    end
  end

  protected

  def valid_permission!
    authorize :game, :all?
  end

  def valid_shop_site!
    render_404! unless Setting.shop_site?
  end

  def valid_cash_site!
    render_404! unless Setting.cash_site?
  end

  def valid_not_api_site!
    render_404! if Setting.api_site?
  end

  def valid_not_shop_site!
    render_404! if Setting.shop_site?
  end
end