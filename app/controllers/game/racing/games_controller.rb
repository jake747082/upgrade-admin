class Game::Racing::GamesController < Game::BaseController
  before_action :set_breadcrumbs_path, :valid_not_api_site!
  before_action :set_machine, only: [:edit, :update]
  skip_before_action :valid_shop_site!

  # GET /games/racing/games
  def index
    @racing_games = RacingGame.all
  end

  # GET /games/racing/games/1/edit
  def edit; end

  # PUT/PATCH /racing/machines/1
  def update
    if @racing_game.update(racing_game_params)
      redirect_to game_racing_games_url, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.setting.racing_games'), game_racing_games_path)
  end

  def set_machine
    @racing_game = RacingGame.find(params[:id])
    drop_breadcrumb(@racing_game.game_model_name)
  end

  def racing_game_params
    params.require(:racing_game).permit(:win_speed, :lose_rate_xs, :lose_rate_sm, :lose_rate_md, :lose_rate_lg, :lose_rate_xl,
                                         :lose_trigger_amount_xs, :lose_trigger_amount_sm, :lose_trigger_amount_md, :lose_trigger_amount_lg, :lose_trigger_amount_xl,
                                         :max_mutiple_xs, :max_mutiple_sm, :max_mutiple_md, :max_mutiple_lg, :max_mutiple_xl)
  end
end