class Agent::LevelsController < Agent::BaseController
  # GET /admin/level_agents/:agent_level
  def show
    agent_level = params[:agent_level].to_s.to_sym

    if Agent.agent_levels[agent_level].blank?
      return render_404!
    end

    @agents = Agent.send("list_#{agent_level}").page(params[:page])
    render 'agents/index'
  end
end
