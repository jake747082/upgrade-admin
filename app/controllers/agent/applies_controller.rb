class Agent::AppliesController < Agent::BaseController
  before_action :set_agent, only: :update
  before_action :set_breadcrumbs_path

  def index
    @agents = Agent.list_apply.page(params[:page])
  end

  def update
    respond_to do |format|
      format.js {
        service = Agent::Apply.new(@agent)
        @status = service.apply!

        @agent.create_log(:review, current_admin, remote_ip) if @status
      }
    end
  end

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.agent.apply'), agents_path)
  end

  def valid_permission!
    authorize :agent, :advanced?
  end
end