class Agent::CashTypesController < Agent::BaseController

  def index
    drop_breadcrumb(t('breadcrumbs.agent.cash_types'))

    @agents = Agent.cashes.list_agent.where(reviewing: false).search(params[:query]).page(params[:page])
  end

  def valid_permission!
    authorize :agent, :advanced?
  end
end