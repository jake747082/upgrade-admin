class Agent::WithdrawsController < Agent::BaseController
  include DateRangeFilter
  before_action :valid_permission!, :set_default_date_range, :set_agent

  def index
    drop_breadcrumb(t('breadcrumbs.reports.withdraw.agent', agent: @agent.title), agent_withdraws_path(@agent))

    render 'reports/withdraws/index'
  end

  def export
    setup_form_data
    @withdraw_stats, @withdraw_total = Withdraw::ChildrenReport.call(begin_date: @begin_date, end_date: @end_date, agent: @agent)
    username = @agent.nil? ? current_admin.username : @agent.username
    filename = "#{Setting.title}_#{username}(#{@begin_date}_to_#{@end_date}).xlsx"
    respond_to do |format|
      format.xlsx {
        render xlsx: "reports/withdraws/export_agent", filename: filename
      }
    end
  end

  protected

  def setup_form_data
    @begin_date = params[:begin_date]
    @end_date = params[:end_date]
    @agent = params[:agent] == nil ? nil : Agent.find(params[:agent])
  end

  def valid_permission!
    authorize :report, :all?
  end
end