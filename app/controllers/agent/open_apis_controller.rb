class Agent::OpenApisController < Agent::BaseController
  before_action :set_agent
  before_action :set_control_api_service

  # POST /agents/:agent_id/open_apis
  def create
    if @agent_control_api_service.open! { |resource| resource.create_log(:api_open, current_admin, remote_ip) }
      redirect_to edit_agent_path(@agent), notice: t('flash.agent.open_api.finish')
    else
      redirect_to edit_agent_path(@agent), alert: t('falsh.agent.error')
    end
  end

  # DELETE /agents/:agent_id/open_apis
  def destroy
    if @agent_control_api_service.close! { |resource| resource.create_log(:api_close, current_admin, remote_ip) }
      redirect_to edit_agent_path(@agent), notice: t('flash.agent.close_api.finish')
    else
      redirect_to edit_agent_path(@agent), alert: t('falsh.agent.error')
    end
  end

  protected

  def set_control_api_service
    @agent_control_api_service = Agent::ControlApi.new(@agent)
  end

end
