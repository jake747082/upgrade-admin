class Agent::ChildsController < Agent::BaseController
  before_action :set_parent_agent,
                :set_breadcrumbs_path,
                :not_allow_target_is_final_level_agent

  # GET /agent/1/childs
  def index
    @agents = @parent_agent.children.page(params[:page])
    render 'agents/index'
  end

  # GET /agent/1/childs/new
  def new
    @form = AgentForm::Create.new(@parent_agent.children.new)
    drop_breadcrumb(t('breadcrumbs.agent.new'))
  end

  # POST /agent/1/childs
  def create
    @form = AgentForm::Create.new(@parent_agent.children.new)
    if @form.create(params[:agent])
      redirect_to @form, notice: t('flash.shared.create.finish')
    else
      render :new
    end
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(@parent_agent.title, agent_path(@parent_agent))
  end

  def not_allow_target_is_final_level_agent
    render_404! if @parent_agent.in_final_level?
  end
end
