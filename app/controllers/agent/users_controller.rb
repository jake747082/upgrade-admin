class Agent::UsersController < Agent::BaseController
  before_action :set_agent, :set_breadcrumbs_path

  # GET /agents/1/users
  def index
    drop_breadcrumb(t('breadcrumbs.user.root'))
    @users = @agent.all_users.includes(:agent).page(params[:page]).order("#{sort_column} #{sort_direction}")
    render 'users/index'
  end

  # GET /agents/1/users/new
  def new
    @form = UserForm::Create.new(User.new(agent: @agent))
    drop_breadcrumb("#{t('breadcrumbs.user.new')}")
  end

  # POST /agents/1/users/create
  def create
    agent = params[:user][:agent].to_i == 0 ? @agent : Agent.find(params[:user][:agent].to_i)
    params[:user][:agent] = agent
    @form = UserForm::Create.new(User.new(agent: agent))
    if @form.create(params[:user])
      @form.model.create_log(:create, current_admin, remote_ip)
      redirect_to @form, notice: t('flash.shared.create.finish')
    else
      render :new
    end
  end

  protected

  def set_breadcrumbs_path
    drop_breadcrumb(@agent.title, agent_users_path(@agent))
  end

  def valid_permission!
    authorize :user
  end

  private

  def sortable_columns
    ['username', 'credit - credit_used', 'created_at', 'agents.username', 'current_sign_in_at', 'current_sign_in_ip']
  end
end