class Agent::CommissionLogsController < Agent::BaseController

  def index
    @logs = AgentCommissionLog.search(params[:query]).page(params[:page])
  end

  def show
    @log = AgentCommissionLog.find(params[:id])
    @game_rate = Reports::AgentCommission.game_rate
    @agent = @log.agent
    @begin_date = @log.begin_date.at_beginning_of_month
    @end_date = @log.end_date.at_end_of_month
    @commission_reports = Finance::AgentCommissionView.call(@agent, {begin_date: @begin_date, end_date: @end_date})
    render "agent/commissions/show"
  end

  def valid_permission!
    authorize :agent, :advanced?
  end
end