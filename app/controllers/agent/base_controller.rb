class Agent::BaseController < ApplicationController
  before_action :valid_permission!, :set_agent_breadcrumbs_path

  protected

  def set_agent_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.agent.root'), agents_path)
  end

  def set_parent_agent
    @parent_agent = Agent.find(params[:agent_id])
  end

  def set_agent
    @agent = Agent.find(params[:agent_id] || params[:id])
  end

  def valid_permission!
    authorize :agent, :all?
  end
end
