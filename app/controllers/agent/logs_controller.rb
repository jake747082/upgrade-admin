class Agent::LogsController < Agent::BaseController
  before_action :set_agent, :set_breadcrumbs_path

  # GET /agent/1/logs
  def index
    @logs = PublicActivity::Activity.where(owner: @agent).order("created_at desc").includes(:owner, :trackable).page(params[:page])
  end

  # GET /agent/1/logs/change
  def change
    @logs = PublicActivity::Activity.where(trackable: @agent).order("created_at desc").includes(:owner, :trackable).page(params[:page])
    render 'agent/logs/index'
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(@agent.title)
  end
end