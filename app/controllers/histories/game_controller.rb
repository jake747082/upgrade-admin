class Histories::GameController < Histories::BaseController
  include ApplicationHelper

  def index
    @sum = @bet_forms.histories_sum.first
    @bet_forms = @bet_forms.includes(:game_platform).page(params[:page])
    super
  end

  def game_result
    bet_form_id = params[:bet_form_id]
    bet_form = ThirdPartyBetForm.find(bet_form_id)
    @platform_name = bet_form.game_platform.name
    platform = GamePlatforms::User.new(@platform_name, User.find(bet_form.user_id))

    @game_data = platform.game_result(bet_form)

    respond_to do |format|
      format.js
    end
  end

  def game_list
    respond_to do |format|
      format.json { render json: {game_list: game_list_kind(params['data'])}}
    end
  end

  private

  def set_breadcrumb
    drop_breadcrumb(t('breadcrumbs.histories.game'))
  end

  def set_bet_forms
    @type = params[:type] || 'all'
    @category_id = params[:category]
    @platform = GamePlatform.find_by_name(params[:type])
    @game_platform_id = params[:role][:game_platform_id] ? params[:role][:game_platform_id] : nil if params[:role]
    @game_kind = params[:role][:game_kind] ? params[:role][:game_kind] : nil if params[:role]
    Rails.logger.info @begin_date
    Rails.logger.info @end_date
    @bet_forms = ThirdPartyBetForm.includes(:user, :agent, :director, :shareholder).where(role_search_params).datetime_range(@begin_date, @end_date)
    @bet_forms = @bet_forms.where(game_platform: @platform) unless @type == 'all'
    @bet_forms = @bet_forms.category(@type, params[:category].to_i) if @category_id.present?
  end

  def role_search_params
    params[:role].present? ? params.require(:role).permit(:agent_id, :user_id, :shareholder_id, :director_id, :slot_machine_id, :game_platform_id, :game_kind) : {}
  end
end
