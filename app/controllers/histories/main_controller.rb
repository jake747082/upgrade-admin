class Histories::MainController < Histories::SlotController
    def index
      data = {type: params[:type], begin_date: params[:begin_date], end_date: params[:end_date], role: params[:role]}
      case params[:type]
      when 'little_mary'
        redirect_to histories_little_mary_index_url(data)
      when 'poker'
        redirect_to histories_poker_index_url(data)
      when 'roulette'
        redirect_to histories_roulette_index_url(data)
      when 'fishing_joy'
        redirect_to histories_fishing_joy_index_url(data)
      else
        redirect_to histories_slot_index_url(data)
      end
    end
end
