class Histories::RacingController < Histories::BaseController
  before_action :valid_not_api_site!

  # GET histories/racing/1.js
  def show
    respond_to do |format|
      format.js {
        @bet_forms = RacingBetForm.find(params[:id])
        @sub_bet_forms = @bet_forms.racing_sub_bet_forms
      }
    end
  end

  private

  def set_breadcrumb
    drop_breadcrumb(t('breadcrumbs.histories.fishing_joy'))
  end

  def set_bet_forms
    @bet_forms = RacingBetForm.where(role_search_params).datetime_range(@begin_date, @end_date).includes(:machine, :user, :agent, :director, :shareholder)
  end

  def role_search_params
    params[:role].present? ? params.require(:role).permit(:agent_id, :machine_id, :shareholder_id, :director_id, :slot_machine_id, :racing_schedule_id) : {}
  end
end
