class Histories::SlotController < Histories::BaseController
  before_action :valid_log_permission!

  def index
    data = {type: params[:type], begin_date: params[:begin_date], end_date: params[:end_date], role: params[:role]}
    redirect_to histories_game_index_url(data)
  end

  private

  def set_breadcrumb
    drop_breadcrumb(t('breadcrumbs.histories.slot'))
  end

  def set_bet_forms
    @bet_forms = SlotBetForm.where(role_search_params).datetime_range(@begin_date, @end_date).includes(:machine, :user, :agent, :director, :shareholder)
  end

  def role_search_params
    params[:role].present? ? params.require(:role).permit(:agent_id, :machine_id, :user_id, :shareholder_id, :director_id, :slot_machine_id) : {}
  end

  def valid_log_permission!
    authorize :user, :log?
  end

end
