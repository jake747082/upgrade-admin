class Devise::BaseController < ApplicationController
  before_action :setup_devise_strong_parameters, :set_breadcrumbs_path
  layout :set_layout_by_resource

  protected
    def setup_devise_strong_parameters
      devise_parameter_sanitizer.for(:account_update) { |u|
        u.permit(:password, :password_confirmation, :current_password)
      }
    end

    def set_layout_by_resource
      if controller_name == 'sessions'
        false
      else
        'application'
      end
    end

    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.account'), edit_admin_registration_path) if controller_name == 'registrations'
    end
end