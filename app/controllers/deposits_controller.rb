class DepositsController < Withdraw::BaseController
  # PUT /deposits
  def index
    @withdraws = Withdraw.list_google_play.search(params[:query]).page(params[:page])
  end

  protected

  def set_breadcrumbs
    drop_breadcrumb(t('breadcrumbs.withdraws.deposit.logs'))
  end
end