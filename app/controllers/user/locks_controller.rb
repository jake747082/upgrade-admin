class User::LocksController < User::BaseController
  before_action :set_user

  # GET /users/1/logs
  def create
    @user.update!(lock: true)
    @user.create_log(:lock, current_admin, remote_ip)
    redirect_to user_path(@user), notice: t('flash.user.lock', user: @user.system_id)
  end
  # GET /users/1/logs
  def destroy
    @user.update!(lock: false)
    @user.create_log(:unlock, current_admin, remote_ip)
    redirect_to user_path(@user), notice: t('flash.user.unlock', user: @user.system_id)
  end

  protected

  def valid_permission!
    authorize :user, :update?
  end

end