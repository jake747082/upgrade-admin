class User::Bonuses::LittleMariesController < User::BaseController
  before_action :set_user, :set_jackpot_info

  # GET /users/1/bonus/little_mary/new
  def new
    respond_to do |format|
      format.js {
        @bonus = @user.little_mary_bonuses.new
      }
    end
  end

  # POST /user/1/bonus/little_mary
  def create
    @bonus = @user.little_mary_bonuses.new
    if @bonus.save
      redirect_to game_little_mary_bonuses_url, notice: t('flash.bonus.create.finish')
    else
      render :new
    end
  end

  protected

  def bonus_params
    params.require(:bonus).permit(:bonus_type, :bonus)
  end

  def set_jackpot_info
    @total_jackpot = LittleMaryMachine.total_jackpot
    @game_setting = GameSetting.find(2)
  end

  def valid_permission!
    authorize :system, :all?
  end
end