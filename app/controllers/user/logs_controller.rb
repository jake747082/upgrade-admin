class User::LogsController < User::BaseController
  before_action :set_user

  # GET /users/1/logs
  def index
    drop_breadcrumb(@user.title)
    @logs = @user.change_logs.page(params[:page])
    render 'logs/index'
  end
end