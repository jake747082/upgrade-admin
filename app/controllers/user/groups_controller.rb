class User::GroupsController < User::BaseController
  before_action :valid_permission!
  before_action :set_breadcrumbs_path, only: [:index, :new, :edit]
  before_action :set_groups, only: [:edit, :update, :destroy]

  def index
    @groups = UserGroup.page(params[:page])
  end

  def new
    respond_to do |format|
      format.js {
        @group = UserGroup.new
      }
    end
  end

  def create
    respond_to do |format|
      format.js {
        @group = UserGroup.new(groups_params)
        @status = @group.save

        current_admin.create_log(:add_user_group, current_admin, remote_ip, name: groups_params[:name]) if @status
      }
    end
  end

  def edit
    respond_to do |format|
      format.js
    end
  end

  def update
    respond_to do |format|
      format.js {
        origin_name = @group.name
        @status = @group.update(groups_params)

        current_admin.create_log(:edit_user_group, current_admin, remote_ip, origin_name: origin_name, name: groups_params[:name], id: @group.id) if @status
      }
    end
  end

  def destroy
    if @group.delete
      current_admin.create_log(:delete_user_group, current_admin, remote_ip, name: @group.name, id: @group.id)
      redirect_to user_groups_path, notice: t('flash.news.delete.finish')
    else
      redirect_to user_groups_path, notice: t('flash.news.delete.fail')
    end
  end

  protected
  
  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.user.groups'), user_groups_path)
  end

  def valid_permission!
    authorize :user, :group?
  end

  def set_groups
    @group = UserGroup.find(params[:id])
  end

  def groups_params
    params.require(:user_group).permit(:name)
  end
end
