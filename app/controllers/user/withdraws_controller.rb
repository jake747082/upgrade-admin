class User::WithdrawsController < User::BaseController
  include DateRangeFilter
  before_action :valid_permission!, :set_default_date_range, :set_user

  def index
    drop_breadcrumb(t('breadcrumbs.reports.withdraw.user', user: @user.title), user_withdraws_path(@user))
    render 'user/withdraws/index'
  end

  protected

  def setup_form_data
    @begin_date = params[:begin_date]
    @end_date = params[:end_date]
    @user = params[:user] == nil ? nil : User.find(params[:user])
  end

  def valid_permission!
    authorize :report, :all?
  end
end