class User::DepositsController < User::BaseController
  before_action :set_user, :set_user_form

  # GET /users/1/depisits/new
  def new
    respond_to do |format|
      format.js
    end
  end
  # POST /users/1/depisits
  def create
    respond_to do |format|
      format.js {
        @status = @deposit_form.deposit

        @user_log = @user.create_log(:deposit, current_admin, remote_ip, credit: @deposit_form.deposit_credit, withdraw_id: @deposit_form.withdraw.id, trade_no: @deposit_form.withdraw.trade_no, status: 'transferred') if @status
      }
    end
  end

  private
    def set_user_form
      @deposit_form = UserForm::Deposit.new(@user, params[:deposit] || { deposit_credit: 0 })
    end
end