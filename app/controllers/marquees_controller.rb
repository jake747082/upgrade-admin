class MarqueesController < ApplicationController
    before_action :valid_permission!
    before_action :set_marquee, only: [:edit, :update, :destroy]
    before_action :set_breadcrumbs_path, only: [:index, :new, :edit]
  def index
    if(params[:language])
      @marquee = Marquee.language(params[:language]).order(:order).page(params[:page])
    else
      @marquee = Marquee.all.order(:order).page(params[:page])
    end
  end

  def new
    @marquee = Marquee.new
  end

  def create
    new_params = marquee_params
    new_params[:days].reject!(&:blank?)
    @marquee = Marquee.new(new_params)
    @marquee.order =params[:language]? Marquee.language(params[:language]).maximum(:order) + 1 : Marquee.maximum(:order)
    if @marquee.save
      # save log
      current_admin.create_log(:marquee, current_admin, remote_ip, id: @marquee.id)
      redirect_to marquees_path, notice: t('flash.shared.create.finish')
    else
      render :new
    end
  end

  def edit; end

  def update
    new_params = marquee_params
    new_params[:days].reject!(&:blank?)
    if @marquee.update(new_params)
      current_admin.create_log(:marquee_update, current_admin, remote_ip, id: @marquee.id)
      redirect_to marquees_path, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  def update_orders
    marquees =[]
    if(params[:data])
      params[:data].each do |param|
        marquee = Marquee.find(param[:id])
        marquee.order = param[:order]
        marquee.save
      end
    end
    render json: {success: true}
  end
  
  def destroy
    if @marquee.delete
      current_admin.create_log(:marquee_delete, current_admin, remote_ip, id: @marquee.id)
      redirect_to marquees_path, notice: t('flash.shared.delete.finish')
    else
      redirect_to marquees_path, notice: t('flash.shared.delete.fail')
    end
  end
  protected
  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.marquee'), marquees_path)
  end

  def valid_permission!
    authorize :marquee, :all?
  end

  def set_marquee
    @marquee = Marquee.find(params[:id])
  end

  def marquee_params
    params.require(:marquee).permit(:title, :content,:language, :active, :by_time,:produced_datetime,:expire_datetime,:only,:eveny_week,:begin_time,:stop_time,days: [])
  end

end