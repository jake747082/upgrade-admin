class ChannelsController < ApplicationController
  skip_before_filter :verify_authenticity_token, :check_site_maintenance!

  # POST /channels.json
  def create
    if params[:channel_name].present? && params[:socket_id].present?
      render json: Pusher[params[:channel_name]].authenticate(params[:socket_id])
    else
      render_500!('params error')
    end
  end
end