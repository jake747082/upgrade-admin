class MachinesController < Machine::BaseController
  before_action :set_machine, only: :show

  # GET /machines
  def index
    @machines = Machine.list_active.includes(:agent).search(params[:query]).page(params[:page])
  end

  # GET /machines/1.json
  def show
    respond_to do |format|
      format.js {
        @machine_logs = @machine.cash_trade_logs.limit(10).includes(:trackable)
      }
    end
  end
  # GET /machines/logs
  def logs
    drop_breadcrumb(t('breadcrumbs.machine.logs'))
    @logs = if params[:query].present?
      machines = Machine.where("mac_address LIKE ? OR nickname LIKE ?", "%#{params[:query]}%", "%#{params[:query]}%")
      PublicActivity::Activity.where(trackable: machines).page(params[:page])
    else
      PublicActivity::Activity.where(trackable_type: Machine).page(params[:page])
    end
  end
end