class UsersController < User::BaseController
  include DateRangeFilter
  helper TransferCreditHelper
  before_action :set_user, only: [:show, :lock, :unlock, :edit, :update, :transfer_credit]
  before_action :set_user_breadcrumbs, only: [:show, :edit]
  before_action :valid_update_permission!, only: [:edit, :update]
  before_action :valid_log_permission!, only: [:logs, :transfer_credit]
  before_action :set_user_date, only: [:index]
  before_action :set_default_date_range, only: [:transfer_credit]

  # GET /users
  def index
    @users = User.includes(:agent).search_user(@search_key, @query, @begin_date, @end_date).page(params[:page]).order("#{sort_column} #{sort_direction}")
  end

  # GET /users/1
  def show
    respond_to do |format|
      @agents = @user.agent.self_and_ancestors
      # @today_credits = @user.daily_logs.today
      @cumulative_credits = @user.cumulative_credits.cal_sum
      @dc_cumulative_credits = @user.cumulative_credits.cal_dc_sum
      format.html
      format.json {
        render "users/show.json"
      }
    end
  end


  # GET /users/1
  def edit
    @form = UserForm::Update.new(@user)
    @user_game_platformship = @user.user_game_platformships.east
  end

  # PUT/PATCH /users/1
  def update
    @form = UserForm::Update.new(@user)
    if @form.update(user_params)
      @user.create_log(:update, current_admin, remote_ip)
      redirect_to @user, notice: t('flash.shared.update.finish', name: @user.system_id)
    else
      render :edit
    end
  end

  # GET /users/logs
  def logs
    drop_breadcrumb(t('breadcrumbs.user.logs'))
    logs = if params[:query].present?
      users = User.where("username LIKE ? OR nickname LIKE ?", "%#{params[:query]}%", "%#{params[:query]}%")
      PublicActivity::Activity.where(trackable: users).page(params[:page])
    else
      PublicActivity::Activity.where(trackable_type: User).page(params[:page])
    end
    @logs = []
    logs.each do |log|
      if log.trackable != nil
        @logs.push(log)
      end
    end
    render 'logs/index'
  end

  # GET /users/limit_table
  def limit_table
    respond_to do |format|
      @limittype = params[:limittype].to_i == 1 ? 'video' : 'roulette'
      @east_limit = GamePlatforms::East.config['limit'][@limittype]
      format.js
    end
  end

  def transfer_credit
    user_valid_amount = @user.third_party_bet_forms.user_valid_amount_sum.datetime_range(@begin_date, @end_date)
    @total_valid_amount = user_valid_amount.first.valid_amount
    drop_breadcrumb(t('breadcrumbs.user.transfer_credit')+" "+User.find(params[:user_id]).title)
    set_transfer_credit_type
    @logs = PublicActivity::Activity.where(trackable: params[:user_id], trackable_type: User).where(updated_at: @begin_date..@end_date).where(@status).page(params[:page])
  end

  def get_credit_left
    platform = params[:platform]
    user = User.find(params[:user_id].to_i)
    user_platform = GamePlatforms::User.new(platform.to_s == 'mg' || platform.to_s == 'bbin' ?  "ag_#{platform.to_s}" : platform.to_s, user)
    credit = user_platform.credit_left == false ? t('messages.maintain') : user_platform.credit_left.to_f
    respond_to do |format|
      format.json { render json: credit.to_json }
    end
  end

  protected

  def set_user_date
    @search_key = params[:search_key].present? ? params[:search_key] : ''
    @query = params[:query].present? ? params[:query] : ''
    if params['status'].nil?
      @begin_date = params[:begin_date].present? ? params[:begin_date] : ''
      @end_date = params[:end_date].present? ? params[:end_date] : ''
    else
      @begin_date = (Time.zone.now - 1.day).strftime('%Y-%m-%d') + ' 12:00:00'
      @end_date   = Time.zone.now.strftime('%Y-%m-%d') + ' 11:59:59'
    end
  end

  def valid_update_permission!
    authorize :user, :update?
  end

  def valid_log_permission!
    authorize :user, :log?
  end

  def set_user_breadcrumbs
    drop_breadcrumb(@user.system_id)
  end

  def set_transfer_credit_type
    @status = "(`key` = 'user.transfer_credit') OR (`key` = 'user.deposit' AND `status` = 'transferred') OR (`key` = 'user.cashout' AND `status` IN ('transferred', 'removed', 'failed')) OR (`key` = 'user.apply_cashout' AND `status` IN ('transferred', 'cancelled'))"
  end

  def user_params
    params.require(:user).permit(:username, :email, :current_password, :password, :password_confirmation, :name, :mobile, :qq, :note, :user_group_id)
  end

  private

  def sortable_columns
    ['username', 'credit - credit_used', 'created_at', 'agents.username', 'current_sign_in_at', 'current_sign_in_ip']
  end
end
