$(function(){
  if ($("body").find(".platform_credit").length > 0) {
    platform = ["east", "ag", "bbin", "mg", "cagayan"]
    $.each( platform, function(key, value){
      $.ajax({
        url: "/users/" + $('.platform_credit').data('user') + "/get_credit_left",
        data: {platform: value}
      }).done(function(data) {
        $("." + value).text(data);
      });
    })
  }
})
