# jQuery & Plugins
#= require jquery
#= require jquery_ujs
#= require jquery.ui.all
#= require shared/bootstraps

# Plugins
#= require turbolinks
#= require nprogress
#= require nprogress-turbolinks
#= require zeroclipboard

# Others
#= require shared/pusher

# Charts
#= require chartkick

#= require redactor.min

#= require autocomplete-rails
