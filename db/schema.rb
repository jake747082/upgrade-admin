# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 97) do

  create_table "activities", force: true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "admin_bank_accounts", force: true do |t|
    t.string   "bank_id",                                                     default: ""
    t.string   "sub_bank_name",                                               default: ""
    t.string   "title",                                                       default: ""
    t.string   "account",                                                     default: ""
    t.decimal  "cumulative_amount",  precision: 11,                 scale: 2, default: 0.0
    t.integer  "limit_update_count",                unsigned: true,           default: 0,     null: false
    t.integer  "update_count",                      unsigned: true,           default: 0,     null: false
    t.boolean  "deposit",                                                     default: true,  null: false
    t.boolean  "open",                                                        default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_bank_accounts", ["deposit"], name: "index_admin_bank_accounts_on_deposit", using: :btree
  add_index "admin_bank_accounts", ["open"], name: "index_admin_bank_accounts_on_open", using: :btree

  create_table "admins", force: true do |t|
    t.string   "username",                                      default: "",    null: false
    t.string   "encrypted_password",                            default: "",    null: false
    t.integer  "sign_in_count",                 unsigned: true, default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role_cd",                                       default: 0,     null: false
    t.string   "nickname",           limit: 32,                 default: "",    null: false
    t.boolean  "lock",                                          default: false, null: false
  end

  add_index "admins", ["username"], name: "index_admins_on_username", unique: true, using: :btree

  create_table "ag_sw_transfer_logs", force: true do |t|
    t.string   "transactionType", limit: 10,               null: false
    t.string   "transactionID",   limit: 50,  default: ""
    t.string   "billNo",                      default: ""
    t.string   "playtype",        limit: 10,               null: false
    t.string   "finish",          limit: 10,  default: ""
    t.string   "sessionToken",    limit: 100,              null: false
    t.string   "playname",        limit: 100, default: ""
    t.string   "agentCode",       limit: 10,  default: ""
    t.string   "betTime",         limit: 100, default: ""
    t.string   "platformType",    limit: 10,  default: ""
    t.string   "round",           limit: 10,  default: ""
    t.string   "gametype",        limit: 10,  default: ""
    t.string   "gameCode",        limit: 100, default: ""
    t.string   "tableCode",       limit: 100, default: ""
    t.string   "transactionCode", limit: 10,  default: ""
    t.string   "ticketStatus",    limit: 10,  default: ""
    t.string   "gameResult",                  default: ""
    t.string   "deviceType",      limit: 10,  default: ""
    t.string   "netAmount",                   default: ""
    t.string   "validBetAmount",              default: ""
    t.string   "currency",        limit: 10,  default: ""
    t.string   "value",                       default: ""
    t.string   "betResponse",     limit: 10,  default: ""
    t.string   "val",                         default: ""
    t.string   "settletime",      limit: 100, default: ""
    t.string   "time",                        default: ""
    t.string   "gameId",          limit: 10,  default: ""
    t.string   "roundId",         limit: 100, default: ""
    t.string   "amount",                      default: ""
    t.string   "remark",                      default: ""
    t.string   "eventID",         limit: 10,  default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ag_sw_transfer_logs", ["transactionID"], name: "index_ag_sw_transfer_logs_on_transactionID", using: :btree

  create_table "ag_sw_undone_bet_forms", force: true do |t|
    t.integer  "shareholder_id",                        unsigned: true,           default: 0,     null: false
    t.decimal  "shareholder_win_amount", precision: 14,                 scale: 4, default: 0.0,   null: false
    t.decimal  "shareholder_owe_parent", precision: 14,                 scale: 4, default: 0.0,   null: false
    t.integer  "director_id",                           unsigned: true,           default: 0,     null: false
    t.decimal  "director_win_amount",    precision: 14,                 scale: 4, default: 0.0,   null: false
    t.decimal  "director_owe_parent",    precision: 14,                 scale: 4, default: 0.0,   null: false
    t.integer  "agent_id",                              unsigned: true,           default: 0,     null: false
    t.decimal  "agent_win_amount",       precision: 14,                 scale: 4, default: 0.0,   null: false
    t.decimal  "agent_owe_parent",       precision: 14,                 scale: 4, default: 0.0,   null: false
    t.integer  "user_id",                               unsigned: true,                           null: false
    t.integer  "game_platform_id",                      unsigned: true,                           null: false
    t.decimal  "bet_total_credit",       precision: 14, unsigned: true, scale: 4,                 null: false
    t.decimal  "reward_amount",          precision: 14,                 scale: 4, default: 0.0,   null: false
    t.decimal  "user_credit_diff",       precision: 14,                 scale: 4, default: 0.0,   null: false
    t.string   "product_id"
    t.string   "game_record_id"
    t.string   "order_number",                                                                    null: false
    t.integer  "table_id",                              unsigned: true
    t.integer  "stage",                                 unsigned: true
    t.integer  "inning",                                unsigned: true
    t.string   "game_name_id"
    t.integer  "game_type_id",                          unsigned: true,           default: 1,     null: false
    t.text     "round"
    t.integer  "game_betting_kind",                     unsigned: true
    t.string   "game_betting_content"
    t.integer  "result_type",                           unsigned: true
    t.decimal  "compensate_rate",        precision: 11,                 scale: 2, default: 0.0
    t.decimal  "balance",                precision: 14,                 scale: 4, default: 0.0
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "betting_at"
    t.string   "vendor_id"
    t.decimal  "valid_amount",           precision: 14,                 scale: 4, default: 0.0,   null: false
    t.string   "game_kind"
    t.text     "result"
    t.string   "card"
    t.string   "ip"
    t.text     "response"
    t.text     "extra"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state",                                                           default: "1"
    t.text     "from"
    t.string   "currency",                                                        default: "CNY"
    t.string   "xml_file_path"
  end

  add_index "ag_sw_undone_bet_forms", ["betting_at"], name: "index_ag_sw_undone_bet_forms_on_betting_at", using: :btree
  add_index "ag_sw_undone_bet_forms", ["game_name_id"], name: "index_ag_sw_undone_bet_forms_on_game_name_id", using: :btree
  add_index "ag_sw_undone_bet_forms", ["game_type_id"], name: "index_ag_sw_undone_bet_forms_on_game_type_id", using: :btree
  add_index "ag_sw_undone_bet_forms", ["order_number"], name: "index_ag_sw_undone_bet_forms_on_order_number", unique: true, using: :btree
  add_index "ag_sw_undone_bet_forms", ["user_id"], name: "index_ag_sw_undone_bet_forms_on_user_id", using: :btree
  add_index "ag_sw_undone_bet_forms", ["vendor_id"], name: "index_ag_sw_undone_bet_forms_on_vendor_id", unique: true, using: :btree

  create_table "agent_commission_logs", force: true do |t|
    t.integer  "agent_id",                  unsigned: true,                         null: false
    t.date     "begin_date"
    t.date     "end_date"
    t.decimal  "amount",     precision: 15,                 scale: 2, default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "agent_commission_logs", ["agent_id"], name: "index_agent_commission_logs_on_agent_id", using: :btree

  create_table "agent_commission_reports", force: true do |t|
    t.date     "date"
    t.integer  "type_cd",                   unsigned: true,                         null: false
    t.integer  "agent_id",                  unsigned: true,                         null: false
    t.integer  "count",                     unsigned: true,           default: 0,   null: false
    t.decimal  "total",      precision: 15,                 scale: 2, default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "agent_commission_reports", ["agent_id"], name: "index_agent_commission_reports_on_agent_id", using: :btree
  add_index "agent_commission_reports", ["date", "type_cd", "agent_id"], name: "index_agent_commission_reports_on_date_and_type_cd_and_agent_id", unique: true, using: :btree
  add_index "agent_commission_reports", ["date"], name: "index_agent_commission_reports_on_date", using: :btree

  create_table "agent_sub_accounts", force: true do |t|
    t.integer  "agent_id",                            unsigned: true
    t.string   "username",                                            default: "",    null: false
    t.string   "encrypted_password",                                  default: "",    null: false
    t.integer  "sign_in_count",                       unsigned: true, default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "nickname",                 limit: 32,                 default: "",    null: false
    t.boolean  "lock",                                                default: false, null: false
    t.boolean  "machine_read_permission",                             default: false, null: false
    t.boolean  "machine_write_permission",                            default: false, null: false
    t.boolean  "finances_permission",                                 default: false, null: false
    t.boolean  "agent_read_permission",                               default: false, null: false
    t.boolean  "agent_write_permission",                              default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "agent_sub_accounts", ["username"], name: "index_agent_sub_accounts_on_username", unique: true, using: :btree

  create_table "agent_withdraws", force: true do |t|
    t.string   "order_id",                                                             default: "",  null: false
    t.integer  "cash_type_cd",      limit: 2,                unsigned: true
    t.integer  "agent_id",                                   unsigned: true,           default: 0,   null: false
    t.decimal  "points",                      precision: 15, unsigned: true, scale: 2, default: 0.0, null: false
    t.text     "bank_account_info"
    t.text     "note"
    t.text     "admin_note"
    t.string   "ip",                                                                   default: "",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "agents", force: true do |t|
    t.string   "nickname",             limit: 32,                                          default: "",    null: false
    t.string   "username",                                                                 default: "",    null: false
    t.string   "name",                 limit: 32,                                          default: "",    null: false
    t.string   "mobile",               limit: 20,                                          default: "",    null: false
    t.string   "qq",                   limit: 32,                                          default: "",    null: false
    t.string   "email",                limit: 32,                                          default: "",    null: false
    t.integer  "type_cd",              limit: 1,                 unsigned: true,           default: 0,     null: false
    t.integer  "owner_id",                                       unsigned: true
    t.string   "encrypted_password",                                                       default: "",    null: false
    t.integer  "sign_in_count",                                  unsigned: true,           default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.decimal  "points",                          precision: 15, unsigned: true, scale: 2, default: 0.0,   null: false
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.decimal  "credit_max",                      precision: 15, unsigned: true, scale: 2, default: 0.0,   null: false
    t.decimal  "credit_dispatched",               precision: 15, unsigned: true, scale: 2, default: 0.0,   null: false
    t.decimal  "credit_used",                     precision: 15,                 scale: 2, default: 0.0,   null: false
    t.decimal  "casino_ratio",                    precision: 4,  unsigned: true, scale: 1, default: 0.0,   null: false
    t.decimal  "commission_ratio",                precision: 4,  unsigned: true, scale: 1, default: 0.0,   null: false
    t.string   "recommend_code"
    t.integer  "total_users_count",                              unsigned: true,           default: 0,     null: false
    t.integer  "total_agents_count",                             unsigned: true,           default: 0,     null: false
    t.integer  "agents_count",                                   unsigned: true,           default: 0,     null: false
    t.integer  "total_machines_count",                           unsigned: true,           default: 0,     null: false
    t.integer  "parent_id",                                      unsigned: true
    t.integer  "lft",                                            unsigned: true,                           null: false
    t.integer  "rgt",                                            unsigned: true,                           null: false
    t.integer  "agent_level_cd",       limit: 1,                 unsigned: true,           default: 0,     null: false
    t.boolean  "user_permission",                                                          default: true
    t.boolean  "lock",                                                                     default: false, null: false
    t.integer  "reviewing",            limit: 1,                 unsigned: true,           default: 0,     null: false
    t.boolean  "commission_view",                                                          default: false, null: false
    t.datetime "commission_at"
    t.string   "maintain_code",        limit: 10
    t.string   "account_id"
    t.string   "secret_key"
    t.string   "session"
    t.datetime "session_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "agents", ["lft", "rgt"], name: "index_agents_on_lft_and_rgt", using: :btree
  add_index "agents", ["owner_id"], name: "index_agents_on_owner_id", using: :btree
  add_index "agents", ["parent_id"], name: "index_agents_on_parent_id", using: :btree
  add_index "agents", ["rgt"], name: "index_agents_on_rgt", using: :btree
  add_index "agents", ["username"], name: "index_agents_on_username", unique: true, using: :btree

  create_table "bank_accounts", force: true do |t|
    t.string   "bank_id",                          default: ""
    t.string   "account",                          default: ""
    t.string   "security_code",                    default: ""
    t.integer  "accountable_id",   unsigned: true,              null: false
    t.string   "accountable_type",                              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city",                             default: ""
    t.string   "province",                         default: ""
    t.string   "subbranch",                        default: ""
  end

  add_index "bank_accounts", ["accountable_id"], name: "index_bank_accounts_on_accountable_id", using: :btree

  create_table "bng_replies", force: true do |t|
    t.string   "uid"
    t.text     "reply_data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bng_replies", ["uid"], name: "index_bng_replies_on_uid", using: :btree

  create_table "bng_rollback_logs", force: true do |t|
    t.integer  "user_id",         unsigned: true, default: 0,   null: false
    t.string   "session"
    t.string   "uid"
    t.string   "game_id"
    t.string   "game_name"
    t.string   "c_at"
    t.string   "sent_at"
    t.string   "round_id",                        default: "0"
    t.string   "transaction_uid"
    t.string   "bet"
    t.string   "win"
    t.boolean  "round_started"
    t.boolean  "round_finished"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bng_rollback_logs", ["session"], name: "index_bng_rollback_logs_on_session", using: :btree
  add_index "bng_rollback_logs", ["user_id"], name: "index_bng_rollback_logs_on_user_id", using: :btree

  create_table "bng_transfer_logs", force: true do |t|
    t.integer  "user_id",        unsigned: true, default: 0,   null: false
    t.string   "session"
    t.string   "uid"
    t.string   "game_id"
    t.string   "game_name"
    t.string   "c_at"
    t.string   "sent_at"
    t.string   "round_id",                       default: "0"
    t.string   "bet"
    t.string   "win"
    t.boolean  "round_started"
    t.boolean  "round_finished"
    t.text     "bonus_info"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bng_transfer_logs", ["session"], name: "index_bng_transfer_logs_on_session", using: :btree
  add_index "bng_transfer_logs", ["user_id"], name: "index_bng_transfer_logs_on_user_id", using: :btree

  create_table "bng_versions", force: true do |t|
    t.integer "user_id",      unsigned: true, default: 0, null: false
    t.string  "last_updated"
    t.integer "version",                      default: 0
  end

  create_table "bt_transfer_logs", force: true do |t|
    t.string   "order_id",                                                         default: "",  null: false
    t.string   "trans_id",                                                         default: "",  null: false
    t.integer  "cash_type_cd",  limit: 2,                unsigned: true
    t.integer  "transfer_type", limit: 2,                unsigned: true
    t.decimal  "credit",                  precision: 15, unsigned: true, scale: 2, default: 0.0, null: false
    t.decimal  "credit_before",           precision: 15, unsigned: true, scale: 2, default: 0.0, null: false
    t.decimal  "credit_after",            precision: 15, unsigned: true, scale: 2, default: 0.0, null: false
    t.integer  "status",                                 unsigned: true,           default: 0,   null: false
    t.integer  "user_id",                                unsigned: true,           default: 0,   null: false
    t.integer  "currency_cd",                            unsigned: true,           default: 0,   null: false
    t.text     "order_info"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bt_transfer_logs", ["cash_type_cd"], name: "index_bt_transfer_logs_on_cash_type_cd", using: :btree
  add_index "bt_transfer_logs", ["order_id"], name: "index_bt_transfer_logs_on_order_id", using: :btree
  add_index "bt_transfer_logs", ["user_id"], name: "index_bt_transfer_logs_on_user_id", using: :btree

  create_table "currencies", force: true do |t|
    t.string   "name",             limit: 10,                          default: "",    null: false
    t.string   "blockchain",       limit: 10,                          default: "",    null: false
    t.string   "token_type",       limit: 10,                          default: "",    null: false
    t.string   "token_coin",       limit: 10,                          default: "",    null: false
    t.decimal  "in_rate",                     precision: 11, scale: 4, default: 0.0,   null: false
    t.decimal  "in_extra_percent",            precision: 11, scale: 4, default: 0.0,   null: false
    t.decimal  "in_extra_limit",              precision: 11, scale: 4, default: 0.0,   null: false
    t.decimal  "in_limit",                    precision: 11, scale: 4, default: 0.0,   null: false
    t.decimal  "out_limit",                   precision: 11, scale: 4, default: 0.0,   null: false
    t.boolean  "in_enable",                                            default: false, null: false
    t.boolean  "out_enable",                                           default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "currencies", ["name", "blockchain"], name: "index_currencies_on_name_and_blockchain", unique: true, using: :btree

  create_table "game_platforms", force: true do |t|
    t.string   "name",             default: "",    null: false
    t.integer  "status_cd",        default: 1
    t.boolean  "is_single_wallet", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "record_id",        default: 0
  end

  create_table "hot_games", force: true do |t|
    t.integer  "game1_id"
    t.integer  "game2_id"
    t.integer  "game3_id"
    t.integer  "game4_id"
    t.integer  "game5_id"
    t.integer  "game6_id"
    t.integer  "game7_id"
    t.integer  "game8_id"
    t.integer  "game9_id"
    t.integer  "game10_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jp_change_logs", force: true do |t|
    t.boolean  "manual"
    t.decimal  "befor_jp_change",        precision: 12,                 scale: 4, default: 0.0
    t.decimal  "after_jp_change",        precision: 12,                 scale: 4, default: 0.0
    t.decimal  "befor_bet_total_credit", precision: 15,                 scale: 4, default: 0.0
    t.decimal  "after_bet_total_credit", precision: 15,                 scale: 4, default: 0.0
    t.decimal  "jp_change",              precision: 12,                 scale: 4, default: 0.0
    t.decimal  "jp_rate",                precision: 5,                  scale: 2
    t.integer  "admin_id",                              unsigned: true
    t.text     "note"
    t.string   "game_platform_name"
    t.integer  "platform_game_id",                      unsigned: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "jp_change_logs", ["platform_game_id"], name: "index_jp_change_logs_on_platform_game_id", using: :btree

  create_table "marquees", force: true do |t|
    t.string   "title",             default: "",    null: false
    t.text     "content",                           null: false
    t.boolean  "active",            default: true,  null: false
    t.boolean  "by_time",           default: false, null: false
    t.datetime "produced_datetime"
    t.datetime "expire_datetime"
    t.boolean  "only",              default: false
    t.boolean  "eveny_week",        default: false, null: false
    t.string   "days"
    t.time     "begin_time"
    t.time     "stop_time"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "language"
    t.integer  "order",             default: 0,     null: false
  end

  create_table "messages", force: true do |t|
    t.string   "title",                       default: "",    null: false
    t.text     "content"
    t.integer  "agent_id",    unsigned: true
    t.integer  "user_id",     unsigned: true
    t.boolean  "deleted",                     default: false, null: false
    t.boolean  "auto",                        default: false, null: false
    t.string   "group_names"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news", force: true do |t|
    t.string   "title",      default: "",    null: false
    t.text     "content"
    t.boolean  "admin_port", default: false, null: false
    t.boolean  "agent_port", default: false, null: false
    t.boolean  "www_port",   default: false, null: false
    t.boolean  "deleted",    default: false, null: false
    t.boolean  "important",  default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pages", force: true do |t|
    t.string   "page_id",    limit: 50,              null: false
    t.string   "title",                 default: "", null: false
    t.text     "content"
    t.integer  "views",                 default: 0,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pages", ["page_id"], name: "index_pages_on_page_id", unique: true, using: :btree

  create_table "platform_games", force: true do |t|
    t.string   "code",                                                                      null: false
    t.string   "game_type",                                                                 null: false
    t.integer  "game_platform_id",                unsigned: true
    t.string   "tech"
    t.string   "plat"
    t.string   "lang"
    t.boolean  "status",                                                    default: false, null: false
    t.boolean  "maintain",                                                  default: false, null: false
    t.boolean  "enable",                                                    default: false, null: false
    t.string   "nameset"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_jp",                                                     default: true,  null: false
    t.decimal  "jp_rate",          precision: 5,                  scale: 2, default: 0.0
    t.decimal  "jp_md",            precision: 10,                 scale: 4, default: 0.0
    t.decimal  "game_reset",       precision: 15,                 scale: 4, default: 0.0
    t.decimal  "base_jp",          precision: 15,                 scale: 4, default: 0.0
    t.decimal  "bet_total_credit", precision: 15,                 scale: 4, default: 0.0
    t.integer  "multiple",                                                  default: 0,     null: false
    t.boolean  "multiple_display",                                          default: false, null: false
    t.boolean  "play_free",                                                 default: false
  end

  add_index "platform_games", ["code"], name: "index_platform_games_on_code", unique: true, using: :btree
  add_index "platform_games", ["game_platform_id"], name: "index_platform_games_on_game_platform_id", using: :btree

  create_table "read_messages", force: true do |t|
    t.integer  "message_id", unsigned: true, default: 0, null: false
    t.integer  "user_id",    unsigned: true, default: 0, null: false
    t.datetime "read_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "redactor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "redactor_assets", ["assetable_type", "assetable_id"], name: "idx_redactor_assetable", using: :btree
  add_index "redactor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_redactor_assetable_type", using: :btree

  create_table "simple_captcha_data", force: true do |t|
    t.string   "key",        limit: 40
    t.string   "value",      limit: 6
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_captcha_data", ["key"], name: "idx_key", using: :btree

  create_table "site_configs", force: true do |t|
    t.boolean  "cash_maintenance",                           default: false, null: false
    t.boolean  "lobby_maintenance",                          default: false, null: false
    t.boolean  "agent_maintenance",                          default: false, null: false
    t.string   "cash_marquee",                               default: "",    null: false
    t.string   "lobby_marquee",                              default: "",    null: false
    t.string   "agent_marquee",                              default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "bet_total_credit",  precision: 15, scale: 4, default: 0.0
    t.decimal  "jp_primary",        precision: 12, scale: 4, default: 0.0
    t.decimal  "base_jp",           precision: 12, scale: 4, default: 0.0
    t.decimal  "jp_reset",          precision: 15, scale: 4, default: 0.0
    t.decimal  "jp_rate",           precision: 5,  scale: 2, default: 0.0
  end

  create_table "third_party_bet_forms", force: true do |t|
    t.integer  "shareholder_id",                        unsigned: true,           default: 0,     null: false
    t.decimal  "shareholder_win_amount", precision: 14,                 scale: 4, default: 0.0,   null: false
    t.decimal  "shareholder_owe_parent", precision: 14,                 scale: 4, default: 0.0,   null: false
    t.integer  "director_id",                           unsigned: true,           default: 0,     null: false
    t.decimal  "director_win_amount",    precision: 14,                 scale: 4, default: 0.0,   null: false
    t.decimal  "director_owe_parent",    precision: 14,                 scale: 4, default: 0.0,   null: false
    t.integer  "agent_id",                              unsigned: true,           default: 0,     null: false
    t.decimal  "agent_win_amount",       precision: 14,                 scale: 4, default: 0.0,   null: false
    t.decimal  "agent_owe_parent",       precision: 14,                 scale: 4, default: 0.0,   null: false
    t.integer  "user_id",                               unsigned: true,                           null: false
    t.integer  "game_platform_id",                      unsigned: true,                           null: false
    t.decimal  "bet_total_credit",       precision: 14,                 scale: 4, default: 0.0,   null: false
    t.decimal  "reward_amount",          precision: 14,                 scale: 4, default: 0.0,   null: false
    t.decimal  "user_credit_diff",       precision: 14,                 scale: 4, default: 0.0,   null: false
    t.string   "product_id"
    t.string   "game_record_id"
    t.string   "order_number",                                                                    null: false
    t.string   "table_id"
    t.integer  "stage",                                 unsigned: true
    t.integer  "inning",                                unsigned: true
    t.string   "game_name_id"
    t.integer  "game_type_id",                          unsigned: true,           default: 1,     null: false
    t.text     "round"
    t.integer  "game_betting_kind",                     unsigned: true
    t.string   "game_betting_content"
    t.integer  "result_type",                           unsigned: true
    t.decimal  "compensate_rate",        precision: 11,                 scale: 2, default: 0.0
    t.decimal  "balance",                precision: 14,                 scale: 4, default: 0.0,   null: false
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "betting_at"
    t.string   "vendor_id"
    t.decimal  "valid_amount",           precision: 14,                 scale: 4, default: 0.0,   null: false
    t.string   "game_kind"
    t.text     "result"
    t.string   "card"
    t.string   "ip"
    t.text     "response"
    t.text     "extra"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state",                                                           default: "1"
    t.text     "from"
    t.string   "currency",                                                        default: "CNY"
    t.string   "xml_file_path"
  end

  add_index "third_party_bet_forms", ["betting_at", "agent_id"], name: "index_third_party_bet_forms_on_betting_at_and_agent_id", using: :btree
  add_index "third_party_bet_forms", ["betting_at", "director_id"], name: "index_third_party_bet_forms_on_betting_at_and_director_id", using: :btree
  add_index "third_party_bet_forms", ["betting_at", "shareholder_id"], name: "index_third_party_bet_forms_on_betting_at_and_shareholder_id", using: :btree
  add_index "third_party_bet_forms", ["betting_at"], name: "index_third_party_bet_forms_on_betting_at", using: :btree
  add_index "third_party_bet_forms", ["game_name_id"], name: "index_third_party_bet_forms_on_game_name_id", using: :btree
  add_index "third_party_bet_forms", ["game_type_id"], name: "index_third_party_bet_forms_on_game_type_id", using: :btree
  add_index "third_party_bet_forms", ["order_number"], name: "index_third_party_bet_forms_on_order_number", unique: true, using: :btree
  add_index "third_party_bet_forms", ["user_id"], name: "index_third_party_bet_forms_on_user_id", using: :btree
  add_index "third_party_bet_forms", ["vendor_id"], name: "index_third_party_bet_forms_on_vendor_id", unique: true, using: :btree

  create_table "unconfirmed_users", force: true do |t|
    t.boolean  "auth_type_cd",                                       default: false
    t.string   "email",                                              default: ""
    t.string   "mobile",                  limit: 20,                 default: ""
    t.string   "verification_code",       limit: 8,                  default: ""
    t.datetime "verification_send_at"
    t.integer  "verification_send_count",            unsigned: true, default: 0,     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "unconfirmed_users", ["email", "mobile"], name: "index_unconfirmed_users_on_email_and_mobile", unique: true, using: :btree
  add_index "unconfirmed_users", ["email"], name: "index_unconfirmed_users_on_email", using: :btree
  add_index "unconfirmed_users", ["mobile"], name: "index_unconfirmed_users_on_mobile", using: :btree

  create_table "user_cumulative_credits", force: true do |t|
    t.integer  "type_cd",                    unsigned: true,                         null: false
    t.integer  "currency_id",                unsigned: true,           default: 0
    t.integer  "user_id",                    unsigned: true,                         null: false
    t.integer  "count",                      unsigned: true,           default: 0,   null: false
    t.decimal  "total",       precision: 18,                 scale: 4, default: 0.0, null: false
    t.decimal  "extra_total", precision: 19, unsigned: true, scale: 8, default: 0.0, null: false
    t.decimal  "dc_total",    precision: 19, unsigned: true, scale: 8, default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_cumulative_credits", ["type_cd", "currency_id", "user_id"], name: "index_on_type_n_currency_n_user", unique: true, using: :btree
  add_index "user_cumulative_credits", ["user_id"], name: "index_user_cumulative_credits_on_user_id", using: :btree

  create_table "user_daily_credit_logs", force: true do |t|
    t.date     "date"
    t.integer  "type_cd",                    unsigned: true,                         null: false
    t.integer  "currency_id",                unsigned: true,           default: 0
    t.integer  "user_id",                    unsigned: true,                         null: false
    t.integer  "count",                      unsigned: true,           default: 0,   null: false
    t.decimal  "total",       precision: 18,                 scale: 4, default: 0.0, null: false
    t.decimal  "dc_total",    precision: 19, unsigned: true, scale: 8, default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_daily_credit_logs", ["date", "type_cd", "currency_id", "user_id"], name: "index_on_date_n_type_n_currency_n_user", unique: true, using: :btree
  add_index "user_daily_credit_logs", ["date"], name: "index_user_daily_credit_logs_on_date", using: :btree
  add_index "user_daily_credit_logs", ["user_id"], name: "index_user_daily_credit_logs_on_user_id", using: :btree

  create_table "user_game_platformships", force: true do |t|
    t.integer  "user_id",                       null: false
    t.integer  "game_platform_id",              null: false
    t.string   "username",         default: "", null: false
    t.string   "password",         default: "", null: false
    t.string   "session_id",       default: "", null: false
    t.integer  "video_limit"
    t.integer  "roulette_limit"
    t.string   "game_limit",       default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_gametokens", force: true do |t|
    t.integer  "user_id",          unsigned: true
    t.integer  "game_platform_id", unsigned: true
    t.integer  "platform_game_id", unsigned: true
    t.text     "extra"
    t.string   "token"
    t.datetime "expired_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_gametokens", ["game_platform_id"], name: "index_user_gametokens_on_game_platform_id", using: :btree
  add_index "user_gametokens", ["user_id", "platform_game_id"], name: "index_user_gametokens_on_user_id_and_platform_game_id", unique: true, using: :btree

  create_table "user_groups", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_groups", ["name"], name: "index_user_groups_on_name", unique: true, using: :btree

  create_table "user_wallets", force: true do |t|
    t.integer  "user_id",     unsigned: true
    t.string   "address",                     default: ""
    t.integer  "type_cd",     unsigned: true, default: 1,  null: false
    t.integer  "currency_id", unsigned: true, default: 0,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_wallets", ["address"], name: "index_user_wallets_on_address", using: :btree
  add_index "user_wallets", ["user_id", "type_cd", "currency_id"], name: "index_user_wallets_on_user_id_and_type_cd_and_currency_id", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "username",                                                                    default: "",    null: false
    t.boolean  "auth_type_cd",                                                                default: false
    t.string   "email",                                                                       default: ""
    t.string   "mobile",                  limit: 20,                                          default: ""
    t.string   "encrypted_password",                                                          default: "",    null: false
    t.string   "nickname",                limit: 32,                                          default: "",    null: false
    t.string   "qq",                                                                          default: ""
    t.string   "name",                                                                        default: ""
    t.integer  "user_group_id",                                     unsigned: true
    t.string   "auth_token"
    t.string   "verification_code",       limit: 8,                                           default: ""
    t.string   "verification_token",                                                          default: ""
    t.datetime "verification_at"
    t.integer  "verification_send_count",                           unsigned: true,           default: 0,     null: false
    t.datetime "verification_send_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                     unsigned: true,           default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "shareholder_id",                                    unsigned: true,           default: 0,     null: false
    t.integer  "director_id",                                       unsigned: true,           default: 0,     null: false
    t.integer  "agent_id",                                          unsigned: true,           default: 0,     null: false
    t.decimal  "credit",                             precision: 18,                 scale: 4, default: 0.0,   null: false
    t.decimal  "credit_used",                        precision: 18,                 scale: 4, default: 0.0,   null: false
    t.decimal  "cashout_limit",                      precision: 18,                 scale: 4, default: 0.0,   null: false
    t.datetime "cashout_limit_at"
    t.boolean  "lock",                                                                        default: false, null: false
    t.integer  "cashin_count",                                      unsigned: true,           default: 0,     null: false
    t.integer  "cashout_count",                                     unsigned: true,           default: 0,     null: false
    t.decimal  "cashin_total",                       precision: 18,                 scale: 4, default: 0.0,   null: false
    t.decimal  "cashout_total",                      precision: 18,                 scale: 4, default: 0.0,   null: false
    t.decimal  "play_total",                         precision: 18,                 scale: 4, default: 0.0,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "note"
    t.string   "bng_token"
    t.datetime "last_action_at"
  end

  add_index "users", ["agent_id"], name: "index_users_on_agent_id", using: :btree
  add_index "users", ["director_id"], name: "index_users_on_director_id", using: :btree
  add_index "users", ["shareholder_id"], name: "index_users_on_shareholder_id", using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "wallet_logs", force: true do |t|
    t.integer  "type_cd",          limit: 1, unsigned: true, default: 0
    t.string   "order_id",                                                null: false
    t.string   "transaction_hash",                           default: ""
    t.string   "from_address",                               default: ""
    t.string   "to_address",                                 default: ""
    t.string   "amount",                                     default: ""
    t.string   "coin_decimals",                              default: ""
    t.string   "coin_abbr",                                  default: ""
    t.string   "status",                                     default: ""
    t.integer  "withdraw_id",                unsigned: true
    t.text     "info"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wallet_logs", ["order_id", "type_cd"], name: "index_wallet_logs_on_order_id_and_type_cd", using: :btree
  add_index "wallet_logs", ["to_address"], name: "index_wallet_logs_on_to_address", using: :btree
  add_index "wallet_logs", ["withdraw_id"], name: "index_wallet_logs_on_withdraw_id", using: :btree

  create_table "withdraws", force: true do |t|
    t.string   "order_id",                                                                   default: "",  null: false
    t.string   "trans_id",                                                                   default: "",  null: false
    t.integer  "cash_type_cd",            limit: 2,                unsigned: true
    t.integer  "type_cd",                 limit: 2,                unsigned: true
    t.string   "pay_method",                                                                 default: "",  null: false
    t.string   "bank_card_type",                                                             default: "",  null: false
    t.string   "bank_code",                                                                  default: "",  null: false
    t.decimal  "credit",                            precision: 15, unsigned: true, scale: 2, default: 0.0, null: false
    t.decimal  "credit_diff",                       precision: 15, unsigned: true, scale: 2, default: 0.0, null: false
    t.decimal  "credit_actual",                     precision: 15, unsigned: true, scale: 2, default: 0.0, null: false
    t.decimal  "cashout_limit",                     precision: 15, unsigned: true, scale: 2, default: 0.0, null: false
    t.integer  "status",                                           unsigned: true,           default: 0,   null: false
    t.integer  "user_id",                                          unsigned: true,           default: 0,   null: false
    t.integer  "agent_id",                                         unsigned: true
    t.integer  "director_id",                                      unsigned: true
    t.integer  "shareholder_id",                                   unsigned: true
    t.string   "from_address",                                                               default: ""
    t.string   "to_address",                                                                 default: ""
    t.integer  "currency_id",                                      unsigned: true,           default: 0,   null: false
    t.decimal  "amount",                            precision: 19, unsigned: true, scale: 8, default: 0.0, null: false
    t.text     "bank_account_info"
    t.string   "admin_bank_account_info"
    t.string   "deposit_at"
    t.text     "note"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "order_info"
    t.text     "admin_note"
    t.string   "ip",                                                                         default: "",  null: false
  end

  add_index "withdraws", ["cash_type_cd"], name: "index_withdraws_on_cash_type_cd", using: :btree
  add_index "withdraws", ["order_id"], name: "index_withdraws_on_order_id", using: :btree
  add_index "withdraws", ["type_cd"], name: "index_withdraws_on_type_cd", using: :btree
  add_index "withdraws", ["user_id"], name: "index_withdraws_on_user_id", using: :btree

  create_table "wm_bet_return_logs", force: true do |t|
    t.string   "account_name",                                                      default: "",  null: false
    t.decimal  "money",                    precision: 15,                 scale: 2, default: 0.0, null: false
    t.string   "request_date", limit: 100,                                          default: ""
    t.string   "dealid",       limit: 50,                                           default: ""
    t.string   "bet_type",     limit: 50,                                           default: ""
    t.string   "betdetail",    limit: 100,                                          default: ""
    t.string   "gameno",       limit: 100,                                          default: ""
    t.string   "code",         limit: 50,                                           default: ""
    t.string   "category",     limit: 50,                                           default: ""
    t.integer  "user_id",                                 unsigned: true,           default: 0,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wm_bet_return_logs", ["user_id"], name: "index_wm_bet_return_logs_on_user_id", using: :btree

  create_table "wm_transfer_logs", force: true do |t|
    t.string   "account_name",                                                       default: "",  null: false
    t.decimal  "money",                     precision: 15,                 scale: 2, default: 0.0, null: false
    t.string   "request_date",  limit: 100,                                          default: ""
    t.string   "dealid",        limit: 50,                                           default: ""
    t.string   "gtype",         limit: 50,                                           default: ""
    t.string   "bet_type",      limit: 50,                                           default: ""
    t.string   "betdetail",     limit: 100,                                          default: ""
    t.string   "gameno",        limit: 100,                                          default: ""
    t.string   "code",          limit: 50,                                           default: ""
    t.string   "category",      limit: 50,                                           default: ""
    t.integer  "user_id",                                  unsigned: true,           default: 0,   null: false
    t.string   "result_status", limit: 50,                                           default: ""
    t.string   "bet_returned",  limit: 50,                                           default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wm_transfer_logs", ["user_id"], name: "index_wm_transfer_logs_on_user_id", using: :btree

end
