# require 'sidekiq/web'
# require 'sidekiq-cron'
Gambling::Application.routes.draw do
  # Concerns
  concern :loggable do |options|
    resources :logs, only: [:index], module: options[:module] do
      collection do
        get :change
      end
    end
  end

  devise_for :admins, path: :account, skip: [:registrations, :passwords, :confirmations]

  devise_scope :admin do
    resource :registration, only: [:edit, :update],
      path: 'account',
      path_names: { edit: 'edit' },
      controller: 'devise/registrations',
      as: :admin_registration
  end

  # 1. 主畫面
  root 'dashboard#index'

  namespace :dashboard do
    get :site_map
  end

  # 2. 代理管理
  namespace :agent do
    resource :levels, only: :show
    resources :applies, only: [:index, :update]
    resources :cash_types, only: :index
    resources :commission_logs, only: [:index, :show]
  end

  resources :agents, except: :destroy do
    # a. agent's 下線玩家, 機器
    resources :machines, only: :index, module: :agent
    resources :users, only: [:index, :new, :create], module: :agent
    # b. agent's 下線玩家
    resources :childs, only: [:index, :new, :create], module: :agent
    # c. logs
    concerns :loggable, module: :agent
    # d. 財務報表
    resources :finances, only: :index, module: :agent
    # e. lock
    resource :locks, only: [:create, :destroy], module: :agent do
      get :destroy_confirm
      get :parent_lock_notice
    end
    # f. 充提報表
    resources :withdraws, only: :index, module: :agent do
      get :export, on: :collection
    end
    resource :open_apis, only: [:create, :destroy], module: :agent
    resource :banks, only: [:edit, :update], module: :agent
    # 產生推薦碼
    resource :recommend_codes, only: :create, module: :agent
    resource :user_permission, only: [:create, :destroy], module: :agent do
      get :open_confirm
      get :notice
    end
    resource :commission, only: [:show, :update, :destroy], module: :agent do
      get :users
      get :details
    end
    # finances select 取得下層
    get :get_children
  end

  namespace :user do
    # 會員群組
    resources :groups, except: :show
  end

  resources :users, only: [:edit, :update, :index, :show] do
    get :limit_table, on: :collection
    get :get_credit_left
    get :logs, module: :user, on: :collection
    get :transfer_credit
    concerns :loggable, module: :user
    # resources :deposits, only: [:new, :create], module: :user
    resource :locks, only: [:create, :destroy], module: :user
    resource :banks, only: [:edit, :update], module: :user
    namespace :bonuses, module: :user do
      resource :slot, only: [:new, :create], module: :bonuses
      resource :little_mary, only: [:new, :create], module: :bonuses
    end
    # 充提報表
    resources :withdraws, only: :index, module: :user
  end
  # 3. 系統設定
  namespace :system do
    # a. 權限管理 (管理員設定)
    resources :admins, except: [:destroy, :show] do
      member do
        post :lock
        post :unlock
      end
      concerns :loggable, module: :admin
    end
    # b. 站台設定
    resource :site_config, only: [:edit, :update]
    # 校正會員累積金額
    resource :cal_cumulative_credits, only: :create

    # 遊戲平台維護設定
    resource :game_platform, only: [:edit, :update]

    resource :hot_game, only: [:edit, :update]
    resources :platform_games, only: [:index,:edit,:update] do
    end
    resource :platform_game,only:[] do
      post :update_all_jp_rate
      post :update_game_list
      put :update_enable
      put :update_play_free
      put :update_is_jp
      post :update_all_jp
    end

    # c. 靜態頁面設定
    resources :pages, only: [:index, :edit, :update]
    # d. 操作記錄
    resources :logs, only: :index
  end

  # 公告
  resources :news, except: :show do
    get :list, on: :collection
    get :detail
  end

  resources :marquees, except: :show do
    post :update_orders, on: :collection
  end

  # 訊息
  resources :messages, except: :show do
    get :autocomplete_user_username, on: :collection
    get :autocomplete_agent_username, on: :collection
  end

  # 4. 機器管理
  resources :machines, only: [:index, :show] do
    get :logs, module: :machine, on: :collection
    concerns :loggable, module: :machine
    resource :bonuses, only: [:new, :create], module: :machine
  end

  # 5. 遊戲設定
  namespace :game do
    root 'slot/machines#index'
    namespace :slot do
      resources :machines, only: [:edit, :update, :index] do
        get :edit_all, on: :collection
        put :update_all, on: :collection
        member do
          get :open
          get :close
        end
      end
      resource :jackpot, only: [:show, :edit, :update]
      resources :bonuses, only: [:index, :destroy]
    end
    namespace :little_mary do
      resources :machines, only: [:edit, :update, :index] do
        member do
          get :open
          get :close
        end
      end
      resource :jackpot, only: [:show, :edit, :update]
      resources :bonuses, only: [:index, :destroy]
    end
    namespace :poker do
      resources :machines, only: [:edit, :update, :index] do
        member do
          get :open
          get :close
        end
      end
    end
    namespace :racing do
      resources :games, only: [:edit, :update, :index]
    end
    namespace :fishing_joy do
      resources :machines, only: [:edit, :update, :index] do
        member do
          get :open
          get :close
        end
      end
    end
    namespace :roulette do
      resources :machines, only: [:edit, :update, :index] do
        member do
          get :open
          get :close
        end
      end
    end
  end

  # 6. pusher channel auth
  resource :channels, only: :create

  # 7. 遊戲歷程
  namespace :histories do
    resources :main, only: :index do
      get :export, on: :collection
    end
    resources :slot, only: :index do
      get :export, on: :collection
    end
    # resources :little_mary, only: :index do
    #   get :export, on: :collection
    # end
    # resources :racing, only: [:index, :show] do
    #   get :export, on: :collection
    # end
    # resources :poker, only: :index do
    #   get :export, on: :collection
    # end
    # resources :fishing_joy, only: :index do
    #   get :export, on: :collection
    # end
    # resources :roulette, only: :index do
    #   get :export, on: :collection
    # end
    resources :game, only: :index do
      post :game_list, on: :collection
      get :export, on: :collection
      get :game_result, on: :collection
    end
    resources :electronic_game, only: :index do
      get :export, on: :collection
    end
  end

  # 8. 財務報表
  resources :finances, only: :index do
    get :export, on: :collection
  end

  mount RedactorRails::Engine => '/redactor_rails'

  namespace :reports do
    # 會員金流報表
    resources :users, only: :index do
      get :details, on: :collection
    end
  end

  # 9. 場次
  namespace :schedules do
    resources :racings, only: :index
  end

  # 10. 提款處理
  resources :withdraws, only: [:index, :show, :update, :destroy] do
    get :logs, on: :collection
  end
  # 儲值處理
  resources :deposits, only: :index

  namespace :withdraw do
    resource :statuses, only: :show do
      post :funpay, on: :collection
    end
    resources :deposits, only: [:new, :show, :create] do
      get :autocomplete_user_username, on: :collection
    end
    resources :cashouts, only: [:new, :show, :create] do
      get :autocomplete_user_username, on: :collection
    end
    # 代理買分
    resources :agent_deposits, only: [:new, :create] do
      get :autocomplete_agent_username, on: :collection
    end
    resources :agent_cashouts, only: [:new, :create] do
      get :autocomplete_agent_username, on: :collection
    end
    resources :agent_withdraws, only: [:index, :show]
  end
  # 產品管理
  resources :products, except: :show do
    resource :disables, only: [:create, :destroy], module: :products
  end

  # 金流管理
  namespace :cash_flow do
    resources :bank_accounts do
      get :cumulative_amount, on: :collection
      patch :edit_cumulative_amount, on: :member
      get :operate_histories, on: :collection
    end
  end
end
