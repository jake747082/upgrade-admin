require 'bundler/capistrano'
require 'rvm/capistrano'
require 'visionbundles'

# RVM Settings
set :rvm_ruby_string, '2.1.0'
set :rvm_type, :user
$:.unshift(File.expand_path('./lib', ENV['rvm_path']))

# Recipes Settings
include_recipes :secret, :nginx, :puma, :db, :dev, :fast_assets

# Role Settings
set :assets_role, [:web, :app]

# Capistrano Base Setting
set :application, 'bt-lobby-admin'
set :user, 'rails'
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false
set :rails_env, :production

# Git Settings
set :scm, :git
set :repository, "git@gitlab.com:btsi365.corp/bt_lobby/admin.git"

# Others
default_run_options[:pty] = true
ssh_options[:forward_agent] = true

# Load from Config
load_config_from "../servers/#{application}", (ARGV[1] || :stagging)

# Deploy Flow
after 'deploy', 'deploy:cleanup' # keep only the last 5 releases

set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"

desc "dev:build task"
task :build, roles: :db, primary: true do
  run "cd #{current_path}; RAILS_ENV=#{rails_env} bundle exec rake dev:build" if dev_sure_danger_command == 'Y'
end