CarrierWave.configure do |config|
  config.asset_host = Setting.redactor.assets_host
  if Setting.redactor.respond_to?(:carrierwave)
    config.fog_credentials = {
      provider: 'AWS',
      aws_access_key_id: Setting.redactor.carrierwave.aws_access_key_id,
      aws_secret_access_key: Setting.redactor.carrierwave.aws_secret_access_key
    }
    config.fog_directory = Setting.redactor.carrierwave.fog_directory
    config.fog_public = true
    config.fog_attributes = { 'Cache-Control' => "max-age=#{365.day.to_i}" }
  end
end