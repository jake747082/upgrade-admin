#!/bin/bash

cd $(dirname $0)/..
app_root=$(pwd)
sidekiq_pidfile="$app_root/tmp/sidekiq.pid"
sidekiq_logfile="$app_root/log/sidekiq.log"
app_user=rails

function stop
{
  bundle exec sidekiqctl stop $sidekiq_pidfile >> $sidekiq_logfile 2>&1
}

function killall
{
  pkill -u $app_user -f sidekiq
}

function restart
{
  if [ -f $sidekiq_pidfile ]; then
    stop
  fi
  killall
  start_sidekiq -d -L $sidekiq_logfile
}

function start_no_deamonize
{
  start_sidekiq
}

function start_sidekiq
{
  cd $app_root
  bundle exec sidekiq -C $app_root/config/sidekiq.yml -L $app_root/log/sidekiq.log -e $RAILS_ENV -P $sidekiq_pidfile $@ >> $sidekiq_logfile 2>&1
}

case "$1" in
  stop)
    stop
    ;;
  start)
    restart
    ;;
  start_no_deamonize)
    start_no_deamonize
    ;;
  restart)
    restart
    ;;
  killall)
    killall
    ;;
  *)
    echo "Usage: RAILS_ENV=your_env $0 {stop|start|start_no_deamonize|restart|killall}"
esac
